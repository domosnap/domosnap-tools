# curanti-user-management


## Prerequire:

*   maven 3
*   OpenJDK 8 (raspberry constraint)

## Structure


*   [domain](https://gitlab.com/domosnap/domosnap-services/${artifactId}/tree/master/domain "domain"): core business implementation about user domain.
*   [infra](https://gitlab.com/domosnap/domosnap-services/${artifactId}/tree/master/infra "infra"): technical implementation (eventStore, repository, etc..)
*   [rest-adapter](https://gitlab.com/domosnap/domosnap-services/${artifactId}/tree/master/rest-adapter "rest-adapter")rest-adapter: external Adapter to communicate with the core busines. In other word: REST API.

## Run

### On your laptop

	mvn clean package
	cd rest-adapter/target
	java -jar rest-adapter-0.x.x-SNAPSHOT.jar -conf ../src/main/configuration/default-configuration.json

### With docker:

    docker login
    docker run registry.gitlab.com/domosnap/domosnap-services/${artifactId}:[tag]

### On rapsberry (with dockerhub and cluster contigured...)

    mvn clean package
    sudo docker build -f Dockerfile_arm32v6 . -t adegiuli/${artifactId}-arm32v6:latest
    sudo docker push adegiuli/${artifactId}-arm32v6:latest
    kubectl delete configmap ${artifactId}
    kubectl create configmap ${artifactId} --from-file=config.json=./rest-adapter/src/main/configuration/test-configuration.json
    kubectl apply -f ${artifactId}-arm32v6.yaml
    

### On GKE from gitlab

On pipeline just lunch the deploy job.

Remark: For new project don't forget to register the token:
    - Create key in gitlab (Settings->Repository->Deploy Tokens)
    - On k8s cluster add the key:

    kubectl create secret docker-registry regsecret-${artifactId} --docker-server=registry.gitlab.com --docker-username='gitlab-ci-token' --docker-password='generated-token'

Don't forget to use imagePullSecret in pod definition: 
    
    imagePullSecrets:
        - name: regsecret-${artifactId}

## Swagger API documentation

[API Documentation](https://app.swaggerhub.com/apis/arnauddegiuli/${artifactId}/1.0.0#/ "API Documentation") 

## SSL
openssl x509 -pubkey -noout -in <certificat.pem> > <fichier de sortie.pub>

&copy;  domosnap.com
