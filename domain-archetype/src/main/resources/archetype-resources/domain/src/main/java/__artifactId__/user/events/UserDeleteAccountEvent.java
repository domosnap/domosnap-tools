#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user.events;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.${artifactId}.user.UserId;

public class UserDeleteAccountEvent extends EventBase<UserId> {

	private final String accountName;

    public UserDeleteAccountEvent(UserId userId, String accountName, ResourceName creator) {
    	super(userId, creator);
        this.accountName = accountName;
    }

    public String getAccountName() {
		return accountName;
	}

}
