#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.logging.Logger;

import com.domosnap.tools.core.services.cqrs.domain.Aggregate;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.impl.ProjectionBase;
import com.domosnap.tools.core.services.exception.AlreadyExistingElementException;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.${artifactId}.user.events.UserChangeEvent;
import ${package}.${artifactId}.user.events.UserCreateAccountEvent;
import ${package}.${artifactId}.user.events.UserCreatedEvent;
import ${package}.${artifactId}.user.events.UserDeleteAccountEvent;
import ${package}.${artifactId}.user.events.UserDeleteEvent;
import ${package}.${artifactId}.user.events.UserRegisteredEvent;

public class User implements Aggregate<UserId> {

	private Logger log = Logger.getLogger(User.class.getName());
	
    private UserProjection projection;

    public static void create(UserId userId, String firstName, String lastName, String photoURL, String email, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	eventPublisher.accept(new UserCreatedEvent(userId, firstName, lastName, photoURL, email, principal));
    }

    public void register(ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	Event<UserId> event = new UserRegisteredEvent(getId(), principal);
    	projection.apply(event);
    	eventPublisher.accept(event);
    }
      
    public User(List<Event<?>> history) {
       projection = new UserProjection(history);
    }
    
    public void changeUserProperties(String firstName, String lastName, String email, String phoneNumber, String photoURL, Instant birthDate, String gender, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	
    	Event<UserId> event = new UserChangeEvent(getId(), firstName, lastName, email, phoneNumber, photoURL, birthDate, gender, principal);
    	projection.apply(event);
    	eventPublisher.accept(event);
    }
    
    public void createAccount(String name, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	boolean exist = false;
    	for (Account account : projection.accountList) {
    		if (account.name.equals(name)) {
    			exist = true;
    			break;
    		}
		}
    	if (!exist) {
    		Event<UserId> event = new UserCreateAccountEvent(getId(), name, principal);
    		projection.apply(event);
    		eventPublisher.accept(event);
    	} else {
    		log.finest("Account [".concat(name).concat("] already existing for user [").concat(String.valueOf(getId())).concat("]."));
    		throw new AlreadyExistingElementException("Account named [".concat(name).concat("]."));
    	}
    }
    
    public void deleteAccount(String name, ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	boolean exist = false;
    	for (Account account : projection.accountList) {
    		if (account.name.equals(name)) {
    			exist = true;
    			break;
    		}
		}
    	if (exist) {
	    	Event<UserId> event = new UserDeleteAccountEvent(getId(), name, principal);
	    	projection.apply(event);
	    	eventPublisher.accept(event);
    	} else {
    		log.finest("Impossible to remove account [".concat(name).concat("] since it doesn't exist for user [").concat(String.valueOf(getId())).concat("]."));
    		throw new NoSuchElementException("Account named [".concat(name).concat("]."));
    	}
    }
        
    public void deleteUser(ResourceName principal, Consumer<Event<?>> eventPublisher) {
    	Event<UserId> event = new UserDeleteEvent(getId(), principal);
    	projection.apply(event);
    	eventPublisher.accept(event);
    }
	
    // QUERY
    public UserId getId() {
        return projection.id;
    }

    public String getFirstName() {
    	return projection.firstName;
    }
    
    public String getLastName() {
    	return projection.lastName;
    }
    
    public String getPhotoURL() {
    	return projection.photoURL;
    }
    
    public String getEmail() {
    	return projection.email;
    }
    
    public String getPhoneNumber() {
    	return projection.phoneNumber;
    }
    
    public Instant getBirthDate() {
    	return projection.birthDate;
    }
    
    public String getGender() {
    	return projection.gender;
    }
    
    public List<UserId> getContactList() {
    	return projection.contactList;
    }
    
    public List<UserId> getPatientList() {
    	return projection.patientList;
    }
    
    public boolean isPatient() {
    	return (projection.type & UserProjection.patient) == UserProjection.patient;
    }
    
    public boolean isContact() {
    	return (projection.type & UserProjection.contact) == UserProjection.contact;
    }
    
    public Account getAccount(String id) {
    	for (Account account : projection.accountList) {
			if (id.equals(account.getId())) {
				return account;
			}
		} 
    	return null;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{${symbol_escape}"id${symbol_escape}":${symbol_escape}"").append(getId()).append("${symbol_escape}",")
          .append("${symbol_escape}"firstName${symbol_escape}":${symbol_escape}"").append(getFirstName()).append("${symbol_escape}",")
          .append("${symbol_escape}"lastName${symbol_escape}":${symbol_escape}"").append(getLastName()).append("${symbol_escape}",")
          .append("${symbol_escape}"email${symbol_escape}":${symbol_escape}"").append(getEmail()).append("${symbol_escape}",")
          .append("${symbol_escape}"phoneNumber${symbol_escape}":${symbol_escape}"").append(getPhoneNumber()).append("${symbol_escape}",")
          .append("${symbol_escape}"photoURL${symbol_escape}":${symbol_escape}"").append(getPhotoURL()).append("${symbol_escape}",")
          .append("${symbol_escape}"birthDate${symbol_escape}":${symbol_escape}"").append(String.valueOf(getBirthDate())).append("${symbol_escape}",")
          .append("${symbol_escape}"gender${symbol_escape}":${symbol_escape}"").append(getGender()).append("${symbol_escape}",")
          .append("${symbol_escape}"contactList${symbol_escape}":{").append(getContactList()).append("}}");

        return sb.toString();
    }
    
    class UserProjection extends ProjectionBase {
    	static final int user = 0x01;
    	static final int contact = 0x02;
    	static final int patient = 0x04;
    	public int type = 0x00;
    	public List<Account> accountList = new ArrayList<>();
    	public List<UserId> contactList = new ArrayList<>();
    	public List<UserId> patientList = new ArrayList<>();
    	public Account selectedAccount;
    	public UserId id;
    	public String firstName;
    	public String lastName;
    	public String email;
    	public String phoneNumber;
    	public String photoURL;
    	public Instant birthDate;
    	public String gender;
    	public boolean deleted = false;
        public List<Event<?>> history = new ArrayList<>();

        public UserProjection(List<Event<?>> history) {
        	register(UserCreatedEvent.class, this::apply);
            register(UserRegisteredEvent.class, this::apply);
            register(UserChangeEvent.class, this::apply);
            register(UserDeleteEvent.class, this::apply);
            register(UserCreateAccountEvent.class, this::apply);
            register(UserDeleteAccountEvent.class, this::apply);
            history.forEach(this::apply);
        }

        public void apply(UserCreatedEvent event) {
        	this.id = event.getAggregateId();
        	this.firstName = event.getFirstName();
        	this.lastName = event.getLastName();
        	this.photoURL = event.getPhotoURL();
        	this.email = event.getEmail();
        	Account account = new Account();
        	selectedAccount = account;
            accountList.add(account);
        	history.add(event);
        }
        
        public void apply(UserRegisteredEvent event) {
        	type = type | UserProjection.user;
        	history.add(event);
        }
        
        public void apply(UserChangeEvent event) {
        	if (event.getNewFirstName() != null) {
        		this.firstName = event.getNewFirstName();
        	}
        	if (event.getNewLastName() != null) {
        		this.lastName = event.getNewLastName();
        	}
        	if (event.getNewEmail() != null) {
        		this.email = event.getNewEmail();
        	}
        	if (event.getNewPhoneNumber() != null) {
        		this.phoneNumber = event.getNewPhoneNumber();
        	}
        	if (event.getNewPhotoURL() != null) {
        		this.photoURL = event.getNewPhotoURL();
        	}
        	if (event.getNewBirthDate() != null) {
        		this.birthDate = event.getNewBirthDate();
        	}
        	if (event.getNewGender() != null) {
        		this.gender = event.getNewGender();
        	}
            history.add(event);
        }
        
        public void apply(UserDeleteEvent event) {
        	deleted = true;
            history.add(event);
        }
        
        public void apply(UserCreateAccountEvent event) {
        	Account account = new Account();
        	account.name = event.getAccountName();
        	accountList.add(account);
        	history.add(event);
        }
        
        public void apply(UserDeleteAccountEvent event) {
        	for (Account account : accountList) {
        		if (event.getAccountName().equals(account.name)) {
        			accountList.remove(account);
        			break;
        		}
    		}
        	history.add(event);
        }
    }

}
