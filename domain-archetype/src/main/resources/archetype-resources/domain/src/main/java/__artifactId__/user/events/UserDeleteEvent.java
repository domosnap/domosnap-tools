#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user.events;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.${artifactId}.user.UserId;

public class UserDeleteEvent extends EventBase<UserId> {

    public UserDeleteEvent(UserId userId, ResourceName creator) {
    	super(userId, creator);
    }
}
