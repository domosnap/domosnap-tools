#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user;

import java.util.UUID;

import com.domosnap.tools.core.services.cqrs.domain.AggregateId;

public class UserId implements AggregateId {
    private String value;

    public UserId(String value) {

        this.value = value;
    }

    public static UserId generate() {
        return new UserId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserId messageId = (UserId) o;

        return !(value != null ? !value.equals(messageId.value) : messageId.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value;
    }

	@Override
	public String getId() {
		return value;
	}
}
