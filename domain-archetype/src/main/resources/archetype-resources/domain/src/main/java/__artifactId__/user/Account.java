#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user;

public class Account {
	
	public String id;
	public String name;
	
	public String getId() {
		return id;
	}
}
