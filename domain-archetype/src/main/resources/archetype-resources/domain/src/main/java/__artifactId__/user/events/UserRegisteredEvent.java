#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user.events;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.${artifactId}.user.UserId;

public class UserRegisteredEvent extends EventBase<UserId> {

    public UserRegisteredEvent(UserId userId, ResourceName creator) {
    	super(userId, creator);
    }
    
}
