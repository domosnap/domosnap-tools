#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user.events;

import java.time.Instant;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.${artifactId}.user.UserId;

public class UserChangeEvent extends EventBase<UserId> {

	private final String newFirstName;
	private final String newLastName;
	private final String newEmail;
	private final String newPhoneNumber;
	private final String newPhotoURL;
	private final Instant newBirthDate;
	private final String newGender;

	public UserChangeEvent(UserId userId, String newFirstName, String newLastName, String newEmail, String newPhoneNumber, String newPhotoURL, Instant newBirthDate, String newGender, ResourceName creator) {
		super(userId, creator);
		this.newFirstName = newFirstName;
		this.newLastName = newLastName;
		this.newEmail = newEmail;
		this.newPhoneNumber = newPhoneNumber;
		this.newPhotoURL = newPhotoURL;
		this.newBirthDate = newBirthDate;
		this.newGender = newGender;
	}

	public String getNewFirstName() {
		return newFirstName;
	}

	public String getNewLastName() {
		return newLastName;
	}

	public String getNewEmail() {
		return newEmail;
	}

	public String getNewPhoneNumber() {
		return newPhoneNumber;
	}

	public String getNewPhotoURL() {
		return newPhotoURL;
	}

	public Instant getNewBirthDate() {
		return newBirthDate;
	}

	public String getNewGender() {
		return newGender;
	}
}
