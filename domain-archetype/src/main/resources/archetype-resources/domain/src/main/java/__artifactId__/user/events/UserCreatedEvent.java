#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.user.events;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.${artifactId}.user.UserId;

public class UserCreatedEvent extends EventBase<UserId> {

	private final String firstName;
	private final String lastName;
	private final String photoURL;
	private final String email;

    public UserCreatedEvent(UserId userId, String firstName, String lastName, String photoURL, String email, ResourceName creator) {
    	super(userId, creator);
        this.firstName = firstName;
        this.lastName = lastName;
        this.photoURL = photoURL;
        this.email = email;
    }
    
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhotoURL() {
		return photoURL;
	}

	public String getEmail() {
		return email;
	}
}
