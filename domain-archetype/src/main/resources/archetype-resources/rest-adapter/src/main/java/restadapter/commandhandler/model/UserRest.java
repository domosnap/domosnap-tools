#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.restadapter.commandhandler.model;

import java.time.Instant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


//TODO see to remove
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserRest {

	@XmlElement
	public String id;
	
	@XmlElement
	public String email;

	@XmlElement
	public String firstName;
	
	@XmlElement
	public String lastName;
	
	@XmlElement
	public String photoURL;
	
	@XmlElement
	public Instant birthDate;
	
	@XmlElement
	public String gender;
	
	@XmlElement
	public String phoneNumber;

}
