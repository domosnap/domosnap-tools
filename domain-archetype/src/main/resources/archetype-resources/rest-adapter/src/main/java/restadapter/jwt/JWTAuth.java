#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.restadapter.jwt;

import java.util.List;

import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
//import io.vertx.ext.auth.jwt.impl.JWTAuthProviderImpl;
import io.vertx.ext.jwt.JWTOptions;

/**
 * Factory interface for creating JWT based {@link io.vertx.ext.auth.AuthProvider} instances.
 *
 * @author Paulo Lopes
 */
@VertxGen
public interface JWTAuth extends AuthProvider {

  /**
   * Create a JWT auth provider
   *
   * @param vertx the Vertx instance
   * @param config  the config
   * @return the auth provider
   */
  @Deprecated
  @GenIgnore
  static JWTAuth create(Vertx vertx, JsonObject config) {
    return create(vertx, new JWTAuthOptions(config));
  }

  /**
   * Create a JWT auth provider
   *
   * @param vertx the Vertx instance
   * @param config  the config
   * @return the auth provider
   */
  static JWTAuth create(Vertx vertx, JWTAuthOptions config) {
    return new JWTAuthProviderImpl(vertx, config);
  }

  /**
   * Generate a new JWT token.
   *
   * @param claims Json with user defined claims for a list of official claims
   *               @see <a href="http://www.iana.org/assignments/jwt/jwt.xhtml">www.iana.org/assignments/jwt/jwt.xhtml</a>
   * @param options extra options for the generation
   *
   * @return JWT encoded token
   */
  String generateToken(JsonObject claims, JWTOptions options);

  /**
   * Rotate keys: remove old one and add new keys.
   * 
   * @param pubSecKeys
   */
  public void rotateKeys(List<PubSecKeyOptions> pubSecKeys);
  
  /**
   * Generate a new JWT token.
   *
   * @param claims Json with user defined claims for a list of official claims
   *               @see <a href="http://www.iana.org/assignments/jwt/jwt.xhtml">www.iana.org/assignments/jwt/jwt.xhtml</a>
   *
   * @return JWT encoded token
   */
  default String generateToken(JsonObject claims) {
    return generateToken(claims, new JWTOptions());
  }
}
