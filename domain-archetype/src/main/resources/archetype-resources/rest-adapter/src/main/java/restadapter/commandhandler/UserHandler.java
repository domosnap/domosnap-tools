#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.restadapter.commandhandler;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.domain.user.User;
import ${package}.domain.user.UserId;
import ${package}.infra.repositories.UserRepository;
import ${package}.restadapter.commandhandler.model.UserJsonCodec;
import ${package}.restadapter.commandhandler.model.UserRest;

import io.vertx.ext.web.RoutingContext;


public class UserHandler {
	
	private Logger log = Logger.getLogger(UserHandler.class.getName());
	
	private UserRepository userRepository;
	private EventPublisher eventPublisher;
	
	
	public UserHandler(EventPublisher eventPublisher, UserRepository userRepository)
	{
		this.eventPublisher = eventPublisher;
		this.userRepository = userRepository;
	}
	
	public void registerUser(RoutingContext rc) {
		
		String userId = rc.user().principal().getString("user_id");
		String name = rc.user().principal().getString("name");
		String picture = rc.user().principal().getString("picture");
		String email = rc.user().principal().getString("email");

		try {
			userRepository.getById(userId);
			// User already exist = we can't register it!
			rc.response()
				.setStatusCode(400)
				.end();
		}
		catch (NoSuchElementException e) { // TODO revoir la gestion du catch ' pas propre
			try {// User doesn't exit = so we can create it!
				Name fullname = new Name(name);
				UserId uid = new UserId(userId);
				ResourceName creator = new ResourceName(ResourceName.DOMAIN_HOUSE, "users", uid.getId());
				User.create(uid, fullname.firstName, fullname.lastName, picture, email, creator, eventPublisher);
				User user = userRepository.getById(userId);
				user.register(creator, eventPublisher);
				
				rc.response()
					.putHeader("Location", rc.normalisedPath() + "/" + userId)
					.putHeader("Content-Type", "application/json; charset=UTF-8")
					.setStatusCode(201)
					.end(UserJsonCodec.toJson(user, userRepository));
				} catch (NoSuchElementException e1) {
					// An error occurs= event store return no entity...
					log.severe(e1.getMessage());
					rc.response().setStatusCode(500).end();
					return;
				}
		}
		
	}
	
	public void getUser(RoutingContext rc) {
		try {
			// TODO add security to not be able to get any user
			String id = rc.pathParam("userId");
			User user = userRepository.getById(id);
			rc.response().putHeader("Content-Type", "application/json;charset=UTF-8")
				.setStatusCode(200)
				.end(UserJsonCodec.toJson(user, userRepository));	
		} catch (NoSuchElementException e) {
			rc.response().setStatusCode(404).end();
			return;
		} catch (IllegalArgumentException e) {
			rc.response().setStatusCode(400).end();
			return;
		}
	}

	public void deleteUser(RoutingContext rc) {
		try {
			String creatorId = rc.user().principal().getString("user_id");
			String id = rc.pathParam("userId");
			User user = userRepository.getById(id);
			ResourceName creator = new ResourceName(ResourceName.DOMAIN_HOUSE, "users", creatorId);
			user.deleteUser(creator, eventPublisher);
			rc.response().setStatusCode(204)
				.end();	
		} catch (NoSuchElementException e) {
			rc.response().setStatusCode(404).end();
			return;
		} catch (IllegalArgumentException e) {
			rc.response().setStatusCode(400).end();
			return;
		}
	}

	public void updateUser(RoutingContext rc) {
		try {
			String creatorId = rc.user().principal().getString("user_id");
			String id = rc.pathParam("userId");
			User user = userRepository.getById(id);
			UserRest ur = UserJsonCodec.fromJson(rc.getBodyAsString());
			ResourceName creator = new ResourceName(ResourceName.DOMAIN_HOUSE, "users", creatorId);
			
			user.changeUserProperties(ur.firstName, ur.lastName, ur.email, ur.phoneNumber, ur.photoURL, ur.birthDate, ur.gender, creator, eventPublisher);
			rc.response().putHeader("Content-Type", "application/json;charset=UTF-8")
				.setStatusCode(204)
				.end();	
		} catch (NoSuchElementException e) {
			rc.response().setStatusCode(404).end();
			return;
		}
	}

	protected class Name {
		public String firstName = null;
		public String lastName = null;
		
		public Name(String name) {
			if (name != null) {
				firstName = name.substring(0, name.indexOf(" "));
				lastName = name.substring(name.indexOf(" "));
			}
		}
	}
}
