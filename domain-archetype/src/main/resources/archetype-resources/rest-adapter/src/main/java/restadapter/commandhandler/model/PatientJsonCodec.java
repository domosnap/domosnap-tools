#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.restadapter.commandhandler.model;

import java.util.List;

import ${package}.domain.user.User;
import ${package}.domain.user.UserId;
import ${package}.infra.repositories.UserRepository;

public class PatientJsonCodec {
	
	public final static String patientListToJson(User user, UserRepository userRepository) {
		return getList(user.getPatientList(), userRepository);
	}
	
	public final static String getList(List<UserId> userIdList, UserRepository userRepository) {
		StringBuilder sb = new StringBuilder("[");
		for (UserId userId : userIdList) {
			User user = userRepository.getById(userId.toString());
			 sb.append("{${symbol_escape}"id${symbol_escape}":${symbol_escape}"").append(user.getId()).append("${symbol_escape}",")
		      .append("${symbol_escape}"firstName${symbol_escape}":${symbol_escape}"").append(user.getFirstName()).append("${symbol_escape}",")
		      .append("${symbol_escape}"lastName${symbol_escape}":${symbol_escape}"").append(user.getLastName()).append("${symbol_escape}",")
		      .append("${symbol_escape}"photoURL${symbol_escape}":${symbol_escape}"").append(user.getPhotoURL()).append("${symbol_escape}"},");
		}
		if (sb.length() > 1) {
			sb.setLength(sb.length()-1);
		}
		
		sb.append("]");
		return sb.toString();
	}	
}
