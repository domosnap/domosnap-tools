#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.restadapter.commandhandler;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import ${package}.domain.user.Account;
import ${package}.domain.user.User;
import ${package}.infra.repositories.UserRepository;

import io.vertx.ext.web.RoutingContext;


public class AccountHandler {
	
	private UserRepository userRepository;
	private EventPublisher eventPublisher;
	
	
	public AccountHandler(EventPublisher eventPublisher, UserRepository userRepository)
	{
		this.eventPublisher = eventPublisher;
		this.userRepository = userRepository;
	}
	
	Logger log = Logger.getLogger(AccountHandler.class.getName());
	
	public void postAccount(RoutingContext rc) {
		
		String userId = rc.get("userId"); //rc.user().principal().getString("user_id");
		String id = rc.get("name");
		
		try {
			
			User user = userRepository.getById(userId);
			ResourceName creator = new ResourceName(ResourceName.DOMAIN_HOUSE, "users", user.getId().getId());
			user.createAccount(id, creator, eventPublisher);
			Account account = user.getAccount(id);
		    
			rc.response()
				.putHeader("Location", rc.normalisedPath() + "/" + id)
				.putHeader("Content-Type", "application/json;charset=UTF-8")
				.setStatusCode(201)
				.end(account.toString());
		}
		catch (NoSuchElementException e) {
				rc.response()
					.setStatusCode(404)
					.end();
				 
		} catch (Exception e1) {
			// An error occurs= event store return no entity...
			rc.response().setStatusCode(500).end();
			return;
		}
		
	}
	

	public void deleteAccount(RoutingContext rc) {
		try {
			String id = rc.pathParam("userId");
			String accountId = rc.pathParam("accountId");
			User user = userRepository.getById(id);
			ResourceName creator = new ResourceName(ResourceName.DOMAIN_HOUSE, "users", user.getId().getId());
			user.deleteAccount(accountId, creator, eventPublisher);
			rc.response().setStatusCode(204)
				.end();	
		} catch (NoSuchElementException e) {
			rc.response().setStatusCode(404).end();
			return;
		} catch (IllegalArgumentException e) {
			rc.response().setStatusCode(400).end();
			return;
		}
	}
}
