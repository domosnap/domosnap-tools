#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.restadapter.commandhandler.model;

import ${package}.domain.user.User;
import ${package}.infra.repositories.UserRepository;

import io.vertx.core.json.JsonObject;

public class UserJsonCodec {
	
	public final static String toJson(User user, UserRepository userRepository) {
		if (user == null) {
			return "";
		}
	    StringBuilder sb = new StringBuilder();
	    sb.append("{${symbol_escape}"id${symbol_escape}":${symbol_escape}"").append(user.getId()).append("${symbol_escape}",")
	      .append("${symbol_escape}"firstName${symbol_escape}":${symbol_escape}"").append(user.getFirstName()).append("${symbol_escape}",")
	      .append("${symbol_escape}"lastName${symbol_escape}":${symbol_escape}"").append(user.getLastName()).append("${symbol_escape}",")
	      .append("${symbol_escape}"email${symbol_escape}":${symbol_escape}"").append(user.getEmail()).append("${symbol_escape}",")
	      .append("${symbol_escape}"phoneNumber${symbol_escape}":${symbol_escape}"").append(user.getPhoneNumber()).append("${symbol_escape}",")
	      .append("${symbol_escape}"photoURL${symbol_escape}":${symbol_escape}"").append(user.getPhotoURL()).append("${symbol_escape}",")
	      .append("${symbol_escape}"birthDate${symbol_escape}":${symbol_escape}"").append(String.valueOf(user.getBirthDate())).append("${symbol_escape}",")
	      .append("${symbol_escape}"gender${symbol_escape}":${symbol_escape}"").append(user.getGender()).append("${symbol_escape}"}");
//	      .append("${symbol_escape}"contactList${symbol_escape}":").append(ContactJsonCodec.contactListToJson(user, userRepository)).append("]}");
	
	    return sb.toString();
	}
	
	public final static UserRest fromJson(String json) {
		if (json == null) {
			return null;
		}
		JsonObject jo = new JsonObject(json);
		UserRest ur = new UserRest();
		ur.id = jo.getString("id");
		ur.firstName = jo.getString("firstName");
		ur.lastName = jo.getString("lastName");
		ur.email=jo.getString("email");
		ur.phoneNumber = jo.getString("phoneNumber");
		ur.photoURL= jo.getString("photoURL");
		ur.birthDate = jo.getInstant("birthDate");
		ur.gender = jo.getString("gender");

		return ur;
	}
}
