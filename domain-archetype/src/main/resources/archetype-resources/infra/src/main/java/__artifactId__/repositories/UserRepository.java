#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.repositories;


import java.util.List;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.repository.AggregateRepository;
import ${package}.domain.user.User;


public class UserRepository extends AggregateRepository<User> {

    public UserRepository(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected User fromHistory(List<Event<?>> history) {
        return new User(history);
    }
}
