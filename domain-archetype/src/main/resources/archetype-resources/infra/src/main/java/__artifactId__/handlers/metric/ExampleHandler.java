#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.handlers.metric;

import java.util.logging.Logger;
import ${package}.domain.user.events.UserChangeEvent;
import ${package}.domain.user.events.UserCreateAccountEvent;
import ${package}.domain.user.events.UserDeleteAccountEvent;
import ${package}.domain.user.events.UserDeleteEvent;
import ${package}.domain.user.events.UserRegisteredEvent;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.handler.SingleEventPublisher;

public class ExampleHandler extends SingleEventPublisher {

	private Logger log = Logger.getLogger(ExampleHandler.class.getName());
	
	protected <T extends Event<?>> void register(Class<T> eventClass) {
		register(eventClass, this::apply);
	}

	public void apply(Event<?> event) {
		try {
//			mixpanel.sendMessage(
//					messageBuilder.event(event.getCreator().getAggregateId(), event.getClass().getSimpleName(), new JSONObject(event)));
		
		} catch (Exception e) {
			log.severe("Metric error: impossible to send event. Details: ".concat(e.getMessage()));
		}
	}

	public ExampleHandler(String projectToken) {
		register(UserChangeEvent.class);
		register(UserCreateAccountEvent.class);
		register(UserDeleteAccountEvent.class);
		register(UserDeleteEvent.class);
		register(UserRegisteredEvent.class);
	}

}
