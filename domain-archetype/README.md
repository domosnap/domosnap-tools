# Domonsap-tools


## Prerequire:

*   maven 3
*   OpenJDK 11

## Maven

Add url of bintray repository in your settings.xml to download curanti core component. See .m2/settings.xml

## Structure


## Build


	mvn clean package
	mvn install

## Run

	mvn archetype:generate -DarchetypeGroupId=com.domosnap.tools -DarchetypeArtifactId=domain-archetype -DarchetypeVersion=0.0.1 -DgroupId=<my.groupid> -DartifactId=<my-artifactId>


## To create a new template from existing project

   mvn archetype:create-from-project -Darchetype.filteredExtensions=java

# Documentation

* [https://maven.apache.org/guides/mini/guide-creating-archetypes.html](https://maven.apache.org/guides/mini/guide-creating-archetypes.html)
* [https://maven.apache.org/archetype/archetype-models/archetype-descriptor/archetype-descriptor.html](https://maven.apache.org/archetype/archetype-models/archetype-descriptor/archetype-descriptor.html) 
* [https://maven.apache.org/archetype/maven-archetype-plugin/examples/create-multi-module-project.html](https://maven.apache.org/archetype/maven-archetype-plugin/examples/create-multi-module-project.html) 


&copy;  Domosnap.com
