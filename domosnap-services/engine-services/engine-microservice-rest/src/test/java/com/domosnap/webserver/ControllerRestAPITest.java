package com.domosnap.webserver;

/*
 * #%L
 * HomeSnapWebServer
 * %%
 * Copyright (C) 2011 - 2016 A. de Giuli
 * %%
 * This file is part of HomeSnap done by A. de Giuli (arnaud.degiuli(at)free.fr).
 * 
 *     MyDomo is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MyDomo is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with MyDomo.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.jboss.resteasy.util.HttpResponseCodes;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.impl.OnOffState;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControllerRestAPITest extends AbstractRestApi {

	private static final String urn_controllers = urn + "/controllers";

	@Before
	public void init() {
		try {
			copyFileUsingFileChannels(new File("Backup of house.xml"), new File("house.xml"));
		} catch (IOException e) {
			Assert.fail("Impossible to initialize file");
		}
	}
	
	@Test
	public void test06GetController() throws UnsupportedEncodingException {
		JSONArray ja = getRequestJSONArray(urn_controllers);
		Assert.assertEquals(29, ja.length()); 
		
		JSONObject jo = getRequestJSONObject(urn_controllers+ "/" + URLEncoder.encode("openwebnet://12345@localhost:1234/15", "UTF-8"));
		testController15(jo);
		
		jo = getRequestJSONObject(urn_controllers+ "/" + URLEncoder.encode("openwebnet://12345@localhost:1234/21", "UTF-8"));
		testController21(jo);
	}

	@Test
	public void test03CreateController() {
		postRequestJSONObject(urn_controllers, createJsonController12(), HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}
	@Test
	public void test09PutController() throws UnsupportedEncodingException {
		JSONObject jo = putRequestJSONObject(urn_controllers + "/" + URLEncoder.encode("openwebnet://12345@localhost:1234/31", "UTF-8"), createJsonController31(), HttpResponseCodes.SC_OK);
		testController31(jo);
		
		jo = putRequestJSONObject(urn_controllers + "/" + URLEncoder.encode("openwebnet://12345@localhost:1234/31", "UTF-8"), createJsonController31bis(), HttpResponseCodes.SC_OK);
		testController31bis(jo);
	}

	@Test
	public void test12DeleteController() throws UnsupportedEncodingException {
		deleteRequestJSONObject(urn_controllers + "/" + URLEncoder.encode("openwebnet://12345@localhost:1234/31", "UTF-8"), HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	
	
	private String createJsonController12() {
		return createJsonController("Chambre Tom", "12", Light.class.getName());
	}

	private void testController15(JSONObject jo) {
		testController(jo, "Miroir Douche", "openwebnet://12345@localhost:1234/15", Light.class.getName(), OnOffState.Off().toString());
	}

	private void testController21(JSONObject jo) {
		testController(jo, "Bureau", "openwebnet://12345@localhost:1234/21", Light.class.getName(), OnOffState.Off().toString());
	}

	private String createJsonController31() {
		return createJsonController("Nvx Cellier", "openwebnet://12345@localhost:1234/31", Light.class.getName());
	}
	
	private void testController31(JSONObject jo) {
		testController(jo, "Nvx Cellier", "openwebnet://12345@localhost:1234/31", Light.class.getName(), OnOffState.Off().toString());
	}

	private String createJsonController31bis() {
		Map<String, String> states = new HashMap<>();
		states.put("status", "on");
		return createJsonController("Nvx Cellier", "openwebnet://12345@localhost:1234/31", Light.class.getName(), states);
	}
	
	private void testController31bis(JSONObject jo) {
		testController(jo, "Nvx Cellier", "openwebnet://12345@localhost:1234/31", Light.class.getName(), OnOffState.On().toString());
	}
}
