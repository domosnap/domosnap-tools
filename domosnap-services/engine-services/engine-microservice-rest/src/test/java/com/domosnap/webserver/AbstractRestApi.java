package com.domosnap.webserver;

/*
 * #%L
 * HomeSnapWebServer
 * %%
 * Copyright (C) 2011 - 2016 A. de Giuli
 * %%
 * This file is part of HomeSnap done by A. de Giuli (arnaud.degiuli(at)free.fr).
 * 
 *     MyDomo is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MyDomo is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with MyDomo.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.jboss.resteasy.util.HttpResponseCodes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;

import com.domosnap.engine.house.Group;
import com.domosnap.engine.house.Label;
import com.domosnap.microservice.engine.ControllerResourceImpl;
import com.domosnap.microservice.engine.GroupResourceImpl;
import com.domosnap.microservice.engine.HouseResourceImpl;
import com.domosnap.microservice.engine.LabelResourceImpl;
import com.domosnap.microservice.engine.tools.JSonTools;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;


public class AbstractRestApi extends Assert {

	
	protected static String host = "localhost";
	protected static String port = "8080";
	protected final static String urn = "/cxf/house";
	private final static String ENDPOINT_ADDRESS = "http://" + host +  ":" + port + urn;
	private final static String WADL_ADDRESS = ENDPOINT_ADDRESS + "?_wadl";
	private static Server server;

	@BeforeClass
	public static void initialize() throws Exception {
		startServer();
		waitForWADL();
	}

	private static void startServer() throws Exception {

		JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
		sf.setResourceClasses(ControllerResourceImpl.class);
		sf.setResourceClasses(HouseResourceImpl.class);
		sf.setResourceClasses(LabelResourceImpl.class);
		sf.setResourceClasses(GroupResourceImpl.class);

		sf.setProvider(new JacksonJaxbJsonProvider());

		List<Object> providers = new ArrayList<Object>();
		// add custom providers if any
		sf.setProviders(providers);

		sf.setResourceProvider(HouseResourceImpl.class, new SingletonResourceProvider(new HouseResourceImpl(), true));
		sf.setAddress(ENDPOINT_ADDRESS);

		System.setProperty("logging.level.org.apache.cxf", "FINE");
		server = sf.create();
	}

	// Optional step - may be needed to ensure that by the time individual
	// tests start running the endpoint has been fully initialized
	@SuppressWarnings("static-access")
	private static void waitForWADL() throws Exception {
		WebClient client = WebClient.create(WADL_ADDRESS);
		// wait for 20 secs or so
		for (int i = 0; i < 20; i++) {
			Thread.currentThread().sleep(1000);
			Response response = client.get();
			if (response.getStatus() == 200) {
				break;
			}
		}
		// no WADL is available yet - throw an exception or give tests a chance
		// to run anyway
	}

	@AfterClass
	public static void destroy() throws Exception {
		server.stop();
		server.destroy();
	}
	
	protected void copyFileUsingFileChannels(File source, File dest) throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		
		try {
			fis = new FileInputStream(source);
			fos = new FileOutputStream(dest);
			inputChannel = fis.getChannel();
			outputChannel = fos.getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		finally {
			fis.close();
			fos.close();
			inputChannel.close();
			outputChannel.close();
		}
	}
	
	protected JSONObject getRequestJSONObject(String urn) {
		Client client = ClientBuilder.newClient();
		String json = null;
		try {
			json = client.target("http://" + host + ":" + port)
					.path(urn)
					.request(MediaType.APPLICATION_JSON)
					.get(String.class);
			return JSonTools.fromJson(json);
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail("Problem with JSON [" + json + "] :" + e.getMessage());
		}
		Assert.fail("Problem when call [" + urn + "].");
		return null;
	}

	protected JSONArray getRequestJSONArray(String urn) {
		Client client = ClientBuilder.newClient();
		String json = null;
		try {
			json = client.target("http://" + host + ":" + port)
					.path(urn)
					.request(MediaType.APPLICATION_JSON)
					.get(String.class);
			return new JSONArray(json); // TODO manage Array null
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail("Problem with JSON [" + json + "] :" + e.getMessage());
			Assert.fail(e.getMessage());
		}
		Assert.fail("Problem when call [" + urn + "].");
		return null;
	}

	
	protected JSONObject postRequestJSONObject(String urn, String body, int returnCodeExpected) {
		return postRequestJSONObject(urn, body, returnCodeExpected, null);
	}

	protected JSONObject postRequestJSONObject(String urn, String body, int returnCodeExpected, String location) {
		Client client = ClientBuilder.newClient();
		String json = null;
		try {
			Response response = client.target("http://" + host + ":" + port)
					.path(urn)
					.request(MediaType.APPLICATION_JSON)
					.post(Entity.entity(body, MediaType.APPLICATION_JSON));
			Assert.assertEquals(returnCodeExpected, response.getStatus());
			if (location != null) {
				assertEquals(location, response.getLocation().getPath());
			}
			if (returnCodeExpected == HttpResponseCodes.SC_METHOD_NOT_ALLOWED || returnCodeExpected == HttpResponseCodes.SC_INTERNAL_SERVER_ERROR || returnCodeExpected == HttpResponseCodes.SC_NOT_IMPLEMENTED || returnCodeExpected == HttpResponseCodes.SC_NOT_ACCEPTABLE|| returnCodeExpected == HttpResponseCodes.SC_BAD_REQUEST) {
				return null;
			} 

			json = response.readEntity(String.class);
			return JSonTools.fromJson(json);
			
		} catch (JSONException e) {
			Assert.fail("Problem with JSON [" + json + "] :" + e.getMessage());
		}
		Assert.fail("Problem when call [" + urn + "].");
		return null;
	}

	protected JSONObject deleteRequestJSONObject(String urn, int returnCodeExpected) {
		Client client = ClientBuilder.newClient();
		Response response = client.target("http://" + host + ":" + port)
					.path(urn)
					.request(MediaType.APPLICATION_JSON)
					.delete();
		Assert.assertEquals(returnCodeExpected, response.getStatus());
		if (returnCodeExpected == HttpResponseCodes.SC_NO_CONTENT  || returnCodeExpected == HttpResponseCodes.SC_NOT_ACCEPTABLE || returnCodeExpected == HttpResponseCodes.SC_NOT_IMPLEMENTED || returnCodeExpected == HttpResponseCodes.SC_METHOD_NOT_ALLOWED) {
			return null;
		}

		String json = null;
		try {
			json = (String) response.getEntity();
			return JSonTools.fromJson(json);
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail("Problem with JSON [" + json + "] :" + e.getMessage());
		}
		return null;
	}

	protected JSONObject putRequestJSONObject(String urn, String body, int returnCodeExpected) {
		Client client = ClientBuilder.newClient();
		String json = null;
		try {
			Response response = client.target("http://" + host + ":" + port)
					.path(urn)
					.request(MediaType.APPLICATION_JSON)
					.put(Entity.entity(body, MediaType.APPLICATION_JSON) );
			Assert.assertEquals(returnCodeExpected, response.getStatus());
			if (returnCodeExpected == HttpResponseCodes.SC_METHOD_NOT_ALLOWED || returnCodeExpected == 	HttpResponseCodes.SC_INTERNAL_SERVER_ERROR || returnCodeExpected == HttpResponseCodes.SC_NOT_IMPLEMENTED || returnCodeExpected == HttpResponseCodes.SC_NOT_ACCEPTABLE|| returnCodeExpected == HttpResponseCodes.SC_BAD_REQUEST) {
				return null;
			}
			json = response.readEntity(String.class);
			return JSonTools.fromJson(json);
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail("Problem with JSON [" + json + "] :" + e.getMessage());
		}
		Assert.fail("Problem when call [" + urn + "].");
		return null;
	}

//	protected JSONObject patchRequestJSONObject(String urn, String body, int returnCodeExpected) {
//		Client client = ClientBuilder.newClient();
//		String json = null;
//		try {
//			Response response = client.target("http://" + host + ":" + port)
//					.path(urn)
//					.request(MediaType.APPLICATION_JSON)
//					.build("PATCH", Entity.entity(body, MediaType.APPLICATION_JSON) )
//					.invoke();
//			Assert.assertEquals(returnCodeExpected, response.getStatus());
//			if (returnCodeExpected == HttpResponseCodes.SC_NOT_IMPLEMENTED || returnCodeExpected == HttpResponseCodes.SC_NOT_ACCEPTABLE || returnCodeExpected == HttpResponseCodes.SC_INTERNAL_SERVER_ERROR) {
//				return null;
//			}
//			json = (String) response.getEntity();
//			return JSonTools.fromJson(json);
//		} catch (JSONException e) {
//			e.printStackTrace();
//			Assert.fail("Problem with JSON [" + json + "] :" + e.getMessage());
//		}
//		Assert.fail("Problem when call [" + urn + "].");
//		return null;
//	}
	
	protected void testError(JSONObject jo, String errorClassname) {
		JSONArray errorList = jo.getJSONArray(JSonTools.ERROR);
		JSONObject error = errorList.getJSONObject(0);
		Assert.assertEquals(errorClassname, error.getString(JSonTools.ERROR_CLASSNAME));
		
	}

	protected String createJsonController(String title, String where, String who) {
		return createJsonController(title, where, who, null);
	}
	
	protected String createJsonController(String title, String where, String who, Map<String, String> states) {
		String sStates = "";
		if (states != null) {
			sStates = ",\"states\":[";
			for (Entry<String, String> iterable_element : states.entrySet()) {
				sStates += "{\"name\":\"" + iterable_element.getKey() + "\",\"value\":\"" + iterable_element.getValue() + "\"},";
			}
			sStates = sStates.substring(0, sStates.length()-1);
			sStates += "]";
		}
		return "{\"title\":\"" + title + "\",\"where\":\"" + where + "\",\"who\":\"" + who + "\"" + sStates + "}";
	}
	
	
	protected void testController(JSONObject jo, String title, String where, String who, String statusValue) {
		Assert.assertNotNull(jo);
		Assert.assertEquals(title, jo.get("title"));
		Assert.assertEquals(where, jo.get("where"));
		Assert.assertEquals(who, jo.get("who"));
		
		JSONArray status = jo.getJSONArray("states");
		JSONObject jo2;
		boolean found = false;
		for (int i = 0; i<status.length(); i++){
			jo2 = status.getJSONObject(i);
			if ("status".equals(jo2.get("name"))/*StateName.STATUS.getName()*/) {
				Assert.assertEquals(statusValue, jo2.getString("value"));
				found = true;
			}
		}
		
		Assert.assertEquals(true, found);;
	}

	protected String createJsonGroup(String title, String where, String controllersList) {
		return "{\"" + 
				Label.JSON_TITLE + "\":\"" + title + "\",\"" +
				Label.JSON_ID + "\":\"" + where + "\",\""+
				Label.JSON_CONTROLLERS + "\": [" + controllersList + "]" + // Controller are not created... => normal
			"}";
	}

	protected void testGroup(JSONObject group, String where, String title, int controllersNumber) {
		Assert.assertNotNull(group);
		Assert.assertEquals(where, group.get(Group.JSON_ID));
		Assert.assertEquals(title, group.get(Group.JSON_TITLE));
		JSONArray controllers = group.getJSONArray(Group.JSON_CONTROLLERS);
		Assert.assertEquals(controllersNumber, controllers.length());
	}

	protected String createJsonLabel(String title, String where) {
		return "{\"" +
		Label.JSON_TITLE + "\":\"" + title + "\",\"" +
		Label.JSON_ID + "\":\"" + where + "\"" +
		"}";
	}
	
	protected void testLabel(JSONObject label, String id, String title, int controllersNumber) {
		Assert.assertEquals(id, label.get(Label.JSON_ID));
		Assert.assertEquals(title, label.get(Label.JSON_TITLE));
		JSONArray controllers = label.getJSONArray(Label.JSON_CONTROLLERS);
		Assert.assertEquals(controllersNumber, controllers.length());
	}
}
