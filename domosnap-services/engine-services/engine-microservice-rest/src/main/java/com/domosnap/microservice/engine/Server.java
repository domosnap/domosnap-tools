package com.domosnap.microservice.engine;

import static io.vertx.core.http.HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.jboss.resteasy.plugins.server.vertx.VertxRequestHandler;
import org.jboss.resteasy.plugins.server.vertx.VertxResteasyDeployment;
import org.json.JSONObject;

import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.CommandJsonCodec;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.ControllerHelper;
import com.domosnap.microservice.engine.tools.HouseRepository;
import com.domosnap.microservice.engine.tools.JSonTools;
import com.domosnap.microservice.engine.tools.Runner;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import io.reactivex.functions.Consumer;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

public class Server extends AbstractVerticle {

	// Convenience method so you can run it in your IDE
	public static void main(String[] args) {
		Runner.runExample(Server.class);
	}

	@Override
	public void start() throws Exception {


		// Build the Jax-RS hello world deployment
		VertxResteasyDeployment deployment = new VertxResteasyDeployment();
		deployment.start();
		deployment.getRegistry().addPerInstanceResource(HouseResourceImpl.class);
		deployment.getRegistry().addPerInstanceResource(GroupResourceImpl.class);
		deployment.getRegistry().addPerInstanceResource(LabelResourceImpl.class);
		deployment.getRegistry().addPerInstanceResource(ControllerResourceImpl.class);
		deployment.getProviderFactory().registerProvider(JacksonJaxbJsonProvider.class);

		// Start the front end server using the Jax-RS controller
		vertx.createHttpServer().requestHandler(new Handler<HttpServerRequest>() {
			@Override
			public void handle(HttpServerRequest event) {
				event.response().putHeader("Access-Control-Allow-Methods", "*");
				event.response().putHeader(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
				new VertxRequestHandler(vertx, deployment).handle(event);
			}
		}).listen(8080, ar -> {
			System.out.println("Server started on port " + ar.result().actualPort());
		});

		// Connect eventbus to websocket
		SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
		Router router = Router.router(vertx);
		router.route("/command/*").handler(sockJSHandler);
		router.route().handler(CorsHandler.create("*").allowedMethod(HttpMethod.GET));
		BridgeOptions options = new BridgeOptions().addInboundPermitted(new PermittedOptions().setAddressRegex(".*"))
				.addOutboundPermitted(new PermittedOptions().setAddressRegex(".*"));
		sockJSHandler.bridge(options);

		vertx.createHttpServer()
				// with websocket
				.requestHandler(router::accept).listen(8087, ar -> {
					System.out.println("Server started on port " + ar.result().actualPort());
				});


		// Start monitor websocket (controller to web)
		for (Controller controller : HouseRepository.getInstance().getControllers()) {
			new ControllerHelper(controller).addCommandConsumer(new Consumer<Command>() {
				@Override
				public void accept(Command command) {
					vertx.eventBus().publish("domosnap-monitor-topic", command.toString());
					System.out.println("COMMAND ENVOYEE " + command.toString());
				}
			});
		}

		// Start commander websocket (web to controller)
		vertx.eventBus().consumer("domosnap-commander-topic", message -> {
			// {"timestamp":"1538753422143",
			//  "what":[{"name":"local_offset","value":"ON - 1"}],
			//  "where":openwebnet://12345@localhost:1234/2,
			//  "who":"HEATING_ADJUSTMENT",
			//  "type":"WRITE",
			//  "source":{"who":"HEATING_ADJUSTMENT",
			//            "title":"null",
			//            "description":"null",
			//            "where":"openwebnet://12345@localhost:1234/2",
			//            "states":{"measure_temperature":"20.0","local_offset":"ON - 1","status":"HEATING_MODE"}
			//	}
			// }
			JSONObject json = JSonTools.fromJson(String.valueOf(message.body()));
			Controller controller = HouseRepository.getInstance().getController(json.getString("where"));

			ControllerHelper helper = new ControllerHelper(controller);
			helper.pushCommand(CommandJsonCodec.fromJson(String.valueOf(message.body())));

			System.out.println("COMMAND RECUE " + String.valueOf(message.body()));
		});

	}
}
