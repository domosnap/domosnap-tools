package com.domosnap.microservice.engine;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.ControllerHelper;
import com.domosnap.engine.controller.what.State;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/controllers")
@Api(value = "Controller")
public interface ControllerResource {


//	"http[s]://server:port/house/controllers/{id}\n" +

	// TODO: actuellement id != where!!!! Id = Who + where?

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(	
			value = "Return controllers",
			notes = "Return list of controllers.",
			response = RestController.class,
			responseContainer = "List"
			)
		@ApiResponses(value = { 
			@ApiResponse(code = 200,
				message = "Return the list of controllerss"
			)
		})
	public List<RestController> getAllControllers();

	@GET
	@Path("/{controllerId}")
	@ApiOperation(value = "Return the controller")
	@Produces(MediaType.APPLICATION_JSON)
	public RestController getControllerById(@PathParam("controllerId") String id);

	@PUT
	@Path("/{controllerId}")
	@ApiOperation(value = "Update a controller")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RestController putControllerWithEntity(@PathParam("controllerId") String id, @ApiParam(value = "Updated controller object", required = true) RestController controller);
	
	@ApiModel
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public class RestController {
		@XmlElement
		private String where;
		@XmlElement
		private String who;
		@XmlElement
		private String description;
		@XmlElement
		private String title;
		@XmlElement
		private List<RestState> states = new ArrayList<RestState>();

		public RestController() {
		}

		@ApiModelProperty(example="Controller's type")
		public String getWho() {
			return who;
		}

		public void setWho(String who) {
			this.who = who;
		}
		
		@ApiModelProperty(example="Controller's description")
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@ApiModelProperty(example="Controller's title")
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		@ApiModelProperty
		public String getWhere() {
			return where;
		}

		public void setWhere(String where) {
			this.where = where;
		}
	
		@ApiModelProperty
		public List<RestState> getStates() {
			return states;
		}

		public void setStates(List<RestState> status) {
			this.states = status;
		}
		
		public RestController mapFrom(Controller engineController) {
			if (engineController != null) {
				this.setDescription(engineController.getDescription());
				this.setTitle(engineController.getTitle());
				for (String stateName : engineController.getStateMap().keySet()) {
					RestState restState = new RestState();
					restState.name = stateName;
					State<?> value = engineController.getStateMap().get(stateName);
					restState.value = value == null ? "null" : value.toString();
					this.states.add(restState);
				}
				this.setWhere(String.valueOf(engineController.getWhere()));
				this.setWho(engineController.getClass().getName());
			}
				return this;
		}
		
		public RestController mapTo(Controller engineController) {
			if (engineController != null) {
				engineController.setDescription(this.getDescription());
				engineController.setTitle(this.getTitle());
				ControllerHelper helper = new ControllerHelper(engineController);
				for (RestState restState : this.states) {
					helper.setState(restState.name, restState.value);
				}
//			if (engineController.getId() == null) {
//				engineController.setId(restController.getId());
//			}
			}
			return this;
		}
	}
	
	@ApiModel
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public class RestState {
		@XmlElement
		private String name;
		@XmlElement
		private String value;

		@ApiModelProperty(example="State's name")
		public String getName() {
			try {
				return this.name = URLDecoder.decode(name, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				return name;
			}
		}

		public void setName(String name) {
			try {
				this.name = URLEncoder.encode(name, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				this.name = name;
			}
		}

		@ApiModelProperty(example="State's value")
		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
}
