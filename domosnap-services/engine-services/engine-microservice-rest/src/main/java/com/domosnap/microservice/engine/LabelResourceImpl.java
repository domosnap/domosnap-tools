package com.domosnap.microservice.engine;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.domosnap.engine.house.Label;
import com.domosnap.microservice.engine.tools.HouseRepository;
import com.domosnap.microservice.engine.tools.JSonTools;

public class LabelResourceImpl implements LabelResource {

	private HouseRepository hr = HouseRepository.getInstance();

	@Override
	public List<RestLabel> getLabels() {
		List<Label> labels = hr.getHouse().getLabels();
		List<RestLabel> restLabels = new ArrayList<LabelResource.RestLabel>();
		for (Label label : labels) {
			restLabels.add(new RestLabel().mapFrom(label));
		}
		return restLabels;
	}

	@Override
	public Response addLabel(RestLabel body, UriInfo uriInfo) {
		Label l = hr.getLabel(body.getId());
		if (l==null) { // Creation
			l = hr.createLabel(body.getId(), body.getDescription(), body.getTitle());
			UriBuilder builder = uriInfo.getAbsolutePathBuilder().path(l.getId());
			return Response.created(builder.build()).entity(new RestLabel().mapFrom(l)).build();
		}
		else { // Update not supported 
			return Response.status(Status.BAD_REQUEST).entity(JSonTools.formatException("GenericError", "Entity already exist. Use PUT API method to update.")).type(MediaType.APPLICATION_JSON).build();
		}
		
	}

	@Override
	public void deleteLabels() {
		hr.getHouse().getLabels().clear();
	}

	@Override
	public RestLabel getLabel(String id) {
		return new RestLabel().mapFrom(hr.getLabel(id));
	}

	@Override
	public RestLabel updateLabel(String id, RestLabel body) {
		return body.mapTo(hr.getLabel(id));
	}

	@Override
	public void deleteLabel(String id) {
		hr.getHouse().getLabels().remove(hr.getLabel(id));
	}

	@Override
	public RestLabel putControllerInLabel(String labelId, String controllerId) {
		// TODO Auto-generated method stub
		return null;
	}
}
