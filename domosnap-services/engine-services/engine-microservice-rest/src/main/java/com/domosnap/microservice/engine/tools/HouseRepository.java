package com.domosnap.microservice.engine.tools;

import java.util.ArrayList;
import java.util.List;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.house.Group;
import com.domosnap.engine.house.House;
import com.domosnap.engine.house.Label;

public class HouseRepository {

	private House house;
	private final HouseService service = new HouseService();;
	private static HouseRepository repo; 

	private HouseRepository() {
		house = service.readHouse();
	}

	public static HouseRepository getInstance() {
		if (repo == null) {
			repo = new HouseRepository();
		}
		return repo;
	}

	public House getHouse() {
		return house;
	}

	public List<Controller> getControllers() {
		List<Controller> controllers = new ArrayList<Controller>();
		for (Group g: house.getGroups()) {
			for (Controller controller : g.getControllerList()) {
				controllers.add(controller);
			}
		}
		return controllers;
	}
	
	public Controller getController(String where) {
		for (Group g : getHouse().getGroups()) {
			for (Controller controller : g.getControllerList()) {
				if (controller.getWhere().getUri().equals(where)) {
					return controller;
				}
			}
		}
		return null;
	}

	public Controller getControllerByGroup(String groupId, String where) {
		Group g = getGroup(groupId);
		if (g != null) {
			for (Controller controller : g.getControllerList()) {
				if (controller.getWhere().getUri().equals(where)) {
					return controller;
				}
			}
		}
		return null;
	}

	public Label getLabel(String labelId) {
		for (Label label : house.getLabels()) {
			if (label.getId().equals(labelId)) {
				return label;
			}
		}
		return null;
	}

	public Label createLabel(String id, String description, String title) {
		Label l = new Label();
		l.setDescription(description);
		if (id != null) {
			l.setId(id);
		}
		l.setTitle(title);
		house.getLabels().add(l);
		return l;
	}

	public Group getGroup(String groupId) {
		for (Group group : house.getGroups()) {
			if (group.getId().equals(groupId)) {
				return group;
			}
		}
		return null;
	}
	
	public Group createGroup(String id, String description, String title) {
		Group g = new Group();
		g.setDescription(description);
		if (id != null) {
			g.setId(id);
		}
		g.setTitle(title);
		house.getGroups().add(g);
		return g;
	}
}
