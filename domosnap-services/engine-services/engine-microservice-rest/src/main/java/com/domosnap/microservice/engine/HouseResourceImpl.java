package com.domosnap.microservice.engine;

import javax.ws.rs.core.UriBuilder;

import com.domosnap.engine.house.Group;
import com.domosnap.engine.house.Label;
import com.domosnap.microservice.engine.tools.HouseRepository;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
public class HouseResourceImpl implements HouseResource {

	private HouseRepository hr = HouseRepository.getInstance();
	
	@Override
	public House getHouse() {
		
		// TODO finir ici!
		House h = new House();
		h.setTitle("House");
		h.setDescription("DomoSnap House");
		for (Group group : hr.getHouse().getGroups()) {
			h.getGroups().add(UriBuilder.fromResource(GroupResource.class).path(group.getId()).build().toString());
		}
		
		for (Label label : hr.getHouse().getLabels()) {
			h.getLabels().add(UriBuilder.fromResource(LabelResource.class).path(label.getId()).build().toString());
		}
		return h;
	}

	@Override
	public void deleteHouse() {
//		hr.getHouse().getGroups().clear(); => on ne peut supprimer des groupes physique!
		hr.getHouse().getLabels().clear();
	}
}
