package com.domosnap.microservice.engine.tools;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class Runner {

	  private static final String RESTEASY_EXAMPLES_DIR = "rengine-microservice-rest";
	  private static final String RESTEASY_EXAMPLES_JAVA_DIR = RESTEASY_EXAMPLES_DIR + "/src/main/java/";

	  public static void runExample(Class<?> clazz) {
	    runExample(RESTEASY_EXAMPLES_JAVA_DIR, clazz, new VertxOptions().setClustered(false), null);
	  }

	  public static void runExample(String exampleDir, Class<?> clazz, VertxOptions options, DeploymentOptions
	      deploymentOptions) {
	    runExample(exampleDir + clazz.getPackage().getName().replace(".", "/"), clazz.getName(), options, deploymentOptions);
	  }


	  public static void runScriptExample(String prefix, String scriptName, VertxOptions options) {
	    File file = new File(scriptName);
	    String dirPart = file.getParent();
	    String scriptDir = prefix + dirPart;
	    runExample(scriptDir, scriptDir + "/" + file.getName(), options, null);
	  }

	  public static void runExample(String exampleDir, String verticleID, VertxOptions options, DeploymentOptions deploymentOptions) {
	    if (options == null) {
	      // Default parameter
	      options = new VertxOptions();
	    }
	    // Smart cwd detection

	    // Based on the current directory (.) and the desired directory (exampleDir), we try to compute the vertx.cwd
	    // directory:
	    try {
	      // We need to use the canonical file. Without the file name is .
	      File current = new File(".").getCanonicalFile();
	      if (exampleDir.startsWith(current.getName()) && !exampleDir.equals(current.getName())) {
	        exampleDir = exampleDir.substring(current.getName().length() + 1);
	      }
	    } catch (IOException e) {
	      // Ignore it.
	    }

	    System.setProperty("vertx.cwd", exampleDir);
	    Consumer<Vertx> runner = vertx -> {
	      try {
	        if (deploymentOptions != null) {
	          vertx.deployVerticle(verticleID, deploymentOptions);
	        } else {
	          vertx.deployVerticle(verticleID);
	        }
	      } catch (Throwable t) {
	        t.printStackTrace();
	      }
	    };
	    if (options.isClustered()) {
	      Vertx.clusteredVertx(options, res -> {
	        if (res.succeeded()) {
	          Vertx vertx = res.result();
	          runner.accept(vertx);
	        } else {
	          res.cause().printStackTrace();
	        }
	      });
	    } else {
	      Vertx vertx = Vertx.vertx(options);
	      runner.accept(vertx);
	    }
	  }
	}
