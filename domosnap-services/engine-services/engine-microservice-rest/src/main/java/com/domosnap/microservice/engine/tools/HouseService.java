package com.domosnap.microservice.engine.tools;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.domosnap.engine.adapter.ControllerFactory;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.house.Group;
import com.domosnap.engine.house.House;
import com.domosnap.engine.house.Icon;
import com.domosnap.engine.house.Label;
import com.domosnap.microservices.engine.persistence.PersistenceService;
import com.domosnap.microservices.engine.persistence.impl.PersistenceServiceImpl;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class HouseService {

	private static Object lock = new String();
	private static House house = null;
	private PersistenceService persistence = new PersistenceServiceImpl();

	public void saveHouse(House house) {
		synchronized (lock) {
			if (house != null) {
				try {
					persistence.save(house, new FileOutputStream("house.xml"));
				} catch (FileNotFoundException e) {
					// TODO log
					e.printStackTrace();
				} catch (IOException e) {
					// TODO log
					e.printStackTrace();
				}
			}
		}
	}

	public House readHouse() {
		synchronized (lock) {
			if (house == null) {
				try {
					house = persistence.retrieve(new FileInputStream("house.xml")); // TODO revoir ca pour prendre un fichier des ressources!
				} catch (FileNotFoundException e) {
					// TODO backup
					house = buildTempHouse();
				} catch (IOException e) {
					// TODO backup
					house = buildTempHouse();
				}
			}
			return house;
		}
	}
	
//	private House buildKnxTempHouse() {
//		house = new House();
//		
//		Label label = new Label();
//		label.setId("ch1");
//		label.setTitle("Chambre 1");
//		label.setIcon(Icon.chamber);
//		house.getLabels().add(label);
//
//
//		Light li2 = serviceKnx.createController(Light.class, new Where("1/1/1"/*, "1/2/1"*/));
//		li2.setTitle("Light 2");
//		label.add(li2);
//
//		Light li3 = serviceKnx.createController(Light.class, new Where("1/1/2"/*, "1/2/2"*/));
//		li3.setTitle("Light 3");
//		label.add(li3);
//
//		label = new Label();
//		label.setId("ch2");
//		label.setTitle("Chambre 2");
//		label.setIcon(Icon.chamber);
//		house.getLabels().add(label);
//
//		Light li4 = serviceKnx.createController(Light.class, new Where("1/1/3"/*, "1/2/3"*/));
//		li4.setTitle("Light ch2");
//		label.add(li4);
//
//		label = new Label();
//		label.setId("cui");
//		label.setTitle("Cuisine");
//		label.setIcon(Icon.chamber);
//		house.getLabels().add(label);
//
//		Light li5 = serviceKnx.createController(Light.class, new Where("1/1/4"/*, "1/2/4"*/));
//		li5.setTitle("Light Cuisine");
//		label.add(li5);
//
//		Group group1 = new Group();
//		group1.setId("1");
//		group1.setTitle("Group 1");
//		group1.add(li2);
//		group1.add(li3);
//		group1.add(li4);
//		group1.add(li5);
//		house.getGroups().add(group1);
//
//		return house;
//	}

	private House buildTempHouse() {
		house = new House();
		
		Label label = new Label();
		label.setId("ch1");
		label.setTitle("Chambre 1");
		label.setIcon(Icon.chamber);
		house.getLabels().add(label);

		Light li = ControllerFactory.createController(Light.class, new Where("openwebnet://12345@localhost:1234/12"));
		li.setTitle("toto");

		label.add(li);

		Light li2 = ControllerFactory.createController(Light.class, new Where("openwebnet://12345@localhost:1234/13"));
		li2.setTitle("Light 2");
		label.add(li2);

		Light li3 = ControllerFactory.createController(Light.class, new Where("openwebnet://12345@localhost:1234/14"));
		li3.setTitle("Light 3");
		label.add(li3);

		label = new Label();
		label.setId("ch2");
		label.setTitle("Chambre 2");
		label.setIcon(Icon.chamber);
		house.getLabels().add(label);

		Light li4 = ControllerFactory.createController(Light.class, new Where("openwebnet://12345@localhost:1234/15"));
		li4.setTitle("Light ch2");
		label.add(li4);

		label = new Label();
		label.setId("cui");
		label.setTitle("Cuisine");
		label.setIcon(Icon.chamber);
		house.getLabels().add(label);

		Light li5 = ControllerFactory.createController(Light.class, new Where("openwebnet://12345@localhost:1234/16"));
		li5.setTitle("Light Cuisine");
		label.add(li5);

		Group group1 = new Group();
		group1.setId("1");
		group1.setTitle("Group 1");
		group1.add(li);
		group1.add(li2);
		group1.add(li3);
		group1.add(li4);
		group1.add(li5);
		house.getGroups().add(group1);

		return house;
	}
}
