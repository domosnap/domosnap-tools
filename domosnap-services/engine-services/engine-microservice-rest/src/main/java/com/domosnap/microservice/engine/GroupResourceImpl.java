package com.domosnap.microservice.engine;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriBuilderException;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.house.Group;
import com.domosnap.microservice.engine.tools.HouseRepository;

public class GroupResourceImpl implements GroupResource {

	private HouseRepository hr = HouseRepository.getInstance();

	@Override
	public List<RestGroup> getGroups() {
		List<Group> groups = hr.getHouse().getGroups();
		List<RestGroup> restGroups = new ArrayList<GroupResource.RestGroup>();
		for (Group group : groups) {
			restGroups.add(map(group));
		}
		return restGroups;
	}

	@Override
	public RestGroup getGroup(String id) {
		return map(hr.getGroup(id));
	}

	@Override
	public RestGroup updateGroup(String id, RestGroup body) {
		Group l = hr.getGroup(id);
		map(body, l);
		return map(l);
	}

	private RestGroup map(Group engineGroup) {
		if (engineGroup == null)
			return null;
		RestGroup restGroup = new RestGroup();
		map(engineGroup, restGroup);
		return restGroup;
	}
	
	private void map(Group engineGroup, RestGroup restGroup) {
		
		restGroup.setDescription(engineGroup.getDescription());
		List<Controller> l = engineGroup.getControllerList();
		for (Controller controller : l) {
			try {
				restGroup.getControllers().add(UriBuilder.fromResource(ControllerResource.class).path(URLEncoder.encode(controller.getWhere().getUri(), "UTF-8")).build().toString());
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UriBuilderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
//		restGroup.setGroups(groups);
		restGroup.setTitle(engineGroup.getTitle());
		restGroup.setId(engineGroup.getId());
	}
	
	private void map(RestGroup restGroup, Group engineGroup) {
		
		engineGroup.setDescription(restGroup.getDescription());
		List<String> l = restGroup.getControllers();
		for (String controllerURI : l) {
			// TODO renforcer le code ici => s'assurer que c'est bien une URI
			String id = controllerURI.substring(controllerURI.lastIndexOf("/"), controllerURI.length());
			engineGroup.getControllerList().add(hr.getController(id));
		}
//		restGroup.setGroups(groups);
		engineGroup.setTitle(restGroup.getTitle());
		if (engineGroup.getId() == null) {
			engineGroup.setId(restGroup.getId());
		}
		
	}
}
