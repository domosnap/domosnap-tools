package com.domosnap.microservice.engine.recorder.http;

/*
 * #%L
 * recorder-microservice-standalone
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.NetworkInterface;
import java.net.SocketException;

public class LogMessageFactory {

    private static LogMessageFactory instance;

    private  Integer androidVersion;
    private  Integer versionCode;
    private  String versionName;
    private  String phoneModel;
    private  String ip;
    private  String wifi;
    private String env;




    public LogMessage newLogMessage(String tag,String level, String body){
        LogMessage message = new LogMessage(tag,level,body);
        init();
        message.setAndroidVersion(this.androidVersion);
        message.setPhoneModel(this.phoneModel);
        message.setVersionCode(this.versionCode);
        message.setVersionName(this.versionName);
        message.setIp(this.ip);
        message.setWifi(this.wifi);
        message.setEnv(this.env);
        return message;
    }

    private void init(){

            androidVersion = 0;
            versionCode = 0;
            versionName = "Recorder-service";
            phoneModel = "Rasberry";
            env = "TEST";
            wifi = "NONE";
            try {
				ip =  NetworkInterface.getNetworkInterfaces().nextElement().getDisplayName();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				ip="unknown";
			}
    }


    private LogMessageFactory(){
        init();
    }

    public void reInit(){
        init();
    }
    public static LogMessageFactory getInstance(){
        if(instance==null){
            instance = new LogMessageFactory();
        }
        return instance;
    }

}
