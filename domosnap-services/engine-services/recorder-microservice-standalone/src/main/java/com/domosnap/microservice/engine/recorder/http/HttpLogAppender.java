package com.domosnap.microservice.engine.recorder.http;

/*
 * #%L
 * recorder-microservice-standalone
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import com.domosnap.engine.Appender;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpLogAppender implements Appender {
	
	private Log log = new Log(HttpLogAppender.class.getName());
	public static final long period = 6000l;
	private ConcurrentLinkedQueue<LogMessage> logMessages = new ConcurrentLinkedQueue<>();
	private ExecutorService executorService;
	private TimerTask timerTask;

	Timer timer = new Timer();

	public static final String URL = "http://www.domosnap.com/log.php";

	private OkHttpClient httpClient;

	public HttpLogAppender() {
		httpClient = new OkHttpClient();
		timerTask = new TimerTask() {
			@Override
			public void run() {
				
				ArrayList<LogMessage> messages = new ArrayList<LogMessage>();
				while (!logMessages.isEmpty()) {
					messages.add(logMessages.poll());
				}
				if (!messages.isEmpty()) {
					log(messages.toArray(new LogMessage[] {}));
				}
			}
		};
		timer.schedule(timerTask, 0, period);
		log.fine(Session.Other, "Timer lunch.");
	}

	@Override
	public void log(String tag, Level level, String message) {
		String sLevel = level != null ? level.getName() : "TRACE";

		LogMessageFactory messageFactory = LogMessageFactory.getInstance();

		try {
			LogMessage logMessage = messageFactory.newLogMessage(tag, sLevel, message);
			logMessages.add(logMessage);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void shutdown() {
		if (timerTask != null) {
			timerTask.run();
		}
		timer.cancel();
		if (httpClient != null && httpClient.connectionPool() != null) {
			httpClient.connectionPool().evictAll();
			httpClient = null;
		}
	}

	private void log(LogMessage[] messages) {
		Gson gson = new Gson();
		String messageAsJson = gson.toJson(messages);
		RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), messageAsJson);

		final Request request = new Request.Builder().post(requestBody).url(URL).build();
		ExecutorService executor = getExecutorService();

		executor.submit(new Runnable() {
			@Override
			public void run() {

				try {
					Response response = httpClient.newCall(request).execute();
					response.body().close();
				} catch (Exception e) {
					if (log.debug) {
						log.info(Session.Other, e.getMessage());
					}
					e.printStackTrace();
				}
			}
		});
	}

	private ExecutorService getExecutorService() {
		if (executorService == null) {
			executorService = Executors.newFixedThreadPool(10);
		}
		return executorService;

	}

}
