package com.domosnap.microservice.engine.recorder.http;

/*
 * #%L
 * recorder-microservice-standalone
 * %%
 * Copyright (C) 2017 - 2020 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Date;

public class LogMessage {
    protected Integer androidVersion;
    protected Integer versionCode;
    protected String versionName;
    protected String phoneModel;

    protected String ip;
    protected String wifi;
    protected String env;

    protected Date creation;

    protected String tag;
    protected String level;
    protected String body;


     LogMessage(String tag,String level, String message){
        this.tag=tag;
        this.level = level;
        this.body = message;
        this.creation = new Date();
    }

     void setIp(String ip) {
        this.ip = ip;
    }

    void setAndroidVersion(Integer androidVersion) {
        this.androidVersion = androidVersion;
    }

    void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
