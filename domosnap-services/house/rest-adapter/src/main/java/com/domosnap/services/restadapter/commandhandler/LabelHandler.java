package com.domosnap.services.restadapter.commandhandler;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.Status;

import com.domosnap.engine.house.Label;
import com.domosnap.microservice.engine.LabelResource.RestLabel;
import com.domosnap.microservice.engine.tools.JSonTools;
import com.domosnap.services.domain.user.House;
import com.domosnap.services.domain.user.exceptions.LabelAlreadyExistingException;
import com.domosnap.services.infra.repositories.HouseRepository;
import com.domosnap.services.restadapter.commandhandler.model.LabelJsonCodec;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;


public class LabelHandler {
	
	private Logger log = Logger.getLogger(LabelHandler.class.getName());
	
	private HouseRepository houseRepository;
	private EventPublisher eventPublisher;
	private String houseId;
	
	public LabelHandler(String houseId, EventPublisher eventPublisher, HouseRepository userRepository) {
		this.houseId = houseId;
		this.eventPublisher = eventPublisher;
		this.houseRepository = userRepository;
	}
	
	public void getLabels(RoutingContext rc) {
		try {
			House house = houseRepository.getById(houseId);
			List<Label> labels = house.getLabels();
			
			rc.response().putHeader("Content-Type", "application/json;charset=UTF-8")
				.setStatusCode(200)
				.end(LabelJsonCodec.toJson(labels));	
		} catch (NoSuchElementException e) {
			log.severe("House not found.");
			rc.response().setStatusCode(404).end();
			return;
		} catch (IllegalArgumentException e) {
			rc.response().setStatusCode(400).end();
			return;
		}
	}

	public void postLabel(RoutingContext rc) {
	
		House house = houseRepository.getById(houseId);
		JsonObject jsonLabel = rc.getBodyAsJson();
		String id = jsonLabel.getString("id");
	
		try {
			Label label = house.addLabel(id, jsonLabel.getString("title"), jsonLabel.getString("description"), null, eventPublisher);
//			UriBuilder builder = uriInfo.getAbsolutePathBuilder().path(l.getId());
//			return Response.created(builder.build()).entity(new RestLabel().mapFrom(l)).build();
		} catch (LabelAlreadyExistingException e) {
			log.fine(e.getMessage());
			return Response.status(Status.BAD_REQUEST).entity(JSonTools.formatException("GenericError", "Entity already exist. Use PUT API method to update.")).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	public void deleteHouse(RoutingContext rc) {
		try {
			String creatorId = rc.user().principal().getString("user_id");
			String id = rc.pathParam("userId");
			House house = houseRepository.getById(id);
			ResourceName creator = new ResourceName(ResourceName.DOMAIN_PROFILE, "users", creatorId);
			house.reset(creator, eventPublisher);
			rc.response().setStatusCode(204)
				.end();	
		} catch (NoSuchElementException e) {
			rc.response().setStatusCode(404).end();
			return;
		} catch (IllegalArgumentException e) {
			rc.response().setStatusCode(400).end();
			return;
		}
	}

}
