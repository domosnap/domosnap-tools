package com.domosnap.services.restadapter;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class Runner {

	private static final String RESTEASY_EXAMPLES_JAVA_DIR = "clients/src/main/java/";

	private static Vertx vertx;
	
	public static void runExample(Class<?> clazz) {
		String verticleID = clazz.getName();
		String exampleDir = RESTEASY_EXAMPLES_JAVA_DIR + clazz.getPackage().getName().replace(".", "/");
		VertxOptions options = new VertxOptions();
		options.getEventBusOptions().setClustered(false);
		// Smart cwd detection

		// Based on the current directory (.) and the desired directory (exampleDir), we
		// try to compute the vertx.cwd
		// directory:
		try {
			// We need to use the canonical file. Without the file name is .
			File current = new File(".").getCanonicalFile();
			if (exampleDir.startsWith(current.getName()) && !exampleDir.equals(current.getName())) {
				exampleDir = exampleDir.substring(current.getName().length() + 1);
			}
		} catch (IOException e) {
			// Ignore it.
		}

		System.setProperty("vertx.cwd", exampleDir);
		Consumer<Vertx> runner = vertx -> {
			try {
				vertx.deployVerticle(verticleID);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		};
		if (options.getEventBusOptions().isClustered()) {
			Vertx.clusteredVertx(options, res -> {
				if (res.succeeded()) {
					vertx = res.result();
					runner.accept(vertx);
				} else {
					res.cause().printStackTrace();
				}
			});
		} else {
			vertx = Vertx.vertx(options);
			runner.accept(vertx);
			
		}
	}
	
	public static void stop() {
		vertx.close();
	}
}
