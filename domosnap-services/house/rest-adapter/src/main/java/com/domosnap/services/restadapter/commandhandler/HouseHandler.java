package com.domosnap.services.restadapter.commandhandler;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

import com.domosnap.services.domain.user.House;
import com.domosnap.services.infra.repositories.HouseRepository;
import com.domosnap.services.restadapter.commandhandler.model.HouseJsonCodec;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

import io.vertx.ext.web.RoutingContext;


public class HouseHandler {
	
	private Logger log = Logger.getLogger(HouseHandler.class.getName());
	
	private HouseRepository houseRepository;
	private EventPublisher eventPublisher;
	
	
	public HouseHandler(EventPublisher eventPublisher, HouseRepository userRepository)
	{
		this.eventPublisher = eventPublisher;
		this.houseRepository = userRepository;
	}
	
	public void getHouse(RoutingContext rc) {
		try {
			String id = rc.pathParam("houseId"); //TODO
			House house = houseRepository.getById(id);
			
			rc.response().putHeader("Content-Type", "application/json;charset=UTF-8")
				.setStatusCode(200)
				.end(HouseJsonCodec.toJson(house, houseRepository));	
		} catch (NoSuchElementException e) {
			log.severe("House not found.");
			rc.response().setStatusCode(404).end();
			return;
		} catch (IllegalArgumentException e) {
			rc.response().setStatusCode(400).end();
			return;
		}
	}

	public void deleteHouse(RoutingContext rc) {
		try {
			String creatorId = rc.user().principal().getString("user_id");
			String id = rc.pathParam("userId");
			House house = houseRepository.getById(id);
			ResourceName creator = new ResourceName(ResourceName.DOMAIN_PROFILE, "users", creatorId);
			house.reset(creator, eventPublisher);
			rc.response().setStatusCode(204)
				.end();	
		} catch (NoSuchElementException e) {
			rc.response().setStatusCode(404).end();
			return;
		} catch (IllegalArgumentException e) {
			rc.response().setStatusCode(400).end();
			return;
		}
	}

//	public void updateUser(RoutingContext rc) {
//		try {
//			String creatorId = rc.user().principal().getString("user_id");
//			String id = rc.pathParam("userId");
//			House user = houseRepository.getById(id);
//			HouseRest ur = HouseJsonCodec.fromJson(rc.getBodyAsString());
//			String title = rc.getBodyAsJson().getString("title");
//			ResourceName creator = new ResourceName(ResourceName.DOMAIN_HOUSE, "users", creatorId);
//			
//			user.changeHouseProperties(title, creator, eventPublisher);
//			rc.response().putHeader("Content-Type", "application/json;charset=UTF-8")
//				.setStatusCode(204)
//				.end();	
//		} catch (NoSuchElementException e) {
//			rc.response().setStatusCode(404).end();
//			return;
//		}
//	}

}
