package com.domosnap.services.restadapter;

public class ConfigProfile {
	
	public final static String PREFIX = "house.";
	public final static String CONFIG_EVENT_STORE = PREFIX + "eventStore";
	public final static String CONFIG_JWT_PUBLICKEYS = PREFIX + "jwt.publicKeys";
	public final static String CONFIG_JWT_TEST_PUBLICKEY = PREFIX + "jwt.testPublicKey";
}
