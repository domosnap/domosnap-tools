package com.domosnap.services.restadapter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.domosnap.services.infra.handlers.metric.ExampleHandler;
import com.domosnap.services.infra.repositories.HouseRepository;
import com.domosnap.services.restadapter.commandhandler.HouseHandler;
import com.domosnap.services.restadapter.commandhandler.LabelHandler;
import com.domosnap.services.restadapter.jwt.JWTAuth;
import com.domosnap.services.restadapter.jwt.JWTAuthHandler;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStoreFactory;
import com.domosnap.tools.core.services.cqrs.infra.handler.DispatchEventPublisher;
import com.domosnap.tools.core.services.cqrs.infra.handler.PersistingEventPublisher;
import com.domosnap.tools.core.services.jwt.GoogleCertificateCrawler;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.impl.BodyHandlerImpl;

public class ServerProfile extends AbstractVerticle {

	private HouseHandler houseHandler;
	private LabelHandler labelHandler;
	
	private EventStore eventStore;

	private final static Logger log = Logger.getLogger(ServerProfile.class.getName());

	// Convenience method so you can run it in your IDE
	public static void main(String[] args) {
		Runner.runExample(ServerProfile.class);
	}

	@Override
	public void start(Promise<Void> startFuture) throws Exception {
		Future<Void> steps = createInfra().compose(v -> createRouter());
		steps.setHandler(ar -> {
			if (ar.succeeded()) {
				startFuture.complete();
			} else {
				startFuture.fail(ar.cause());
			}
		});
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		eventStore.close();
	}

	private Future<Void> createRouter() {
		Future<Void> future = Future.future(promise -> {
			final Router router = Router.router(vertx);

			// Authentication settings
			final JWTAuthOptions jwtAuthOptions = new JWTAuthOptions();
			jwtAuthOptions.getJWTOptions().setLeeway(10);
			final JWTAuth authProvider = JWTAuth.create(vertx, jwtAuthOptions);

			refreshKeys(authProvider).setHandler(result -> {
				// Healthcheck
				HealthCheckHandler healthCheckHandler = HealthCheckHandler.create(vertx);
				healthCheckHandler.register("ping", result2 -> {
					result2.complete(Status.OK());
				});

				router.route().handler(new BodyHandlerImpl()).failureHandler(new FailureHandler());

				router.route("/house/*").handler(JWTAuthHandler.create(authProvider));

				// house
				router.get("/house/").produces("application/json").handler(houseHandler::getHouse);
				router.delete("/house").handler(houseHandler::deleteHouse);

				// group
				router.get("/house/groups").produces("application/json"); //TODO implement
				router.get("/house/groups/:groupId").produces("application/json"); //TODO implement
				router.put("/house/groups/:groupId").consumes("application/json").produces("application/json"); //TODO implement
				
				// label
				router.get("/house/labels").produces("application/json").handler(labelHandler::getLabels);
				router.post("/house/labels").consumes("application/jaon").produces("application/json").handler(labelHandler::postLabel);
				router.delete("/house/labels"); //TODO implement
				router.get("/house/labels/:labelId").produces("application/json"); //TODO implement
				router.put("/house/labels/:labelId").consumes("application/json").produces("application/json"); //TODO implement
				router.delete("/house/labels/:labelId"); // TODO implement
				router.put("/house/labels/:labelId").produces("application/json"); //TODO implement
				
				// Health
				router.get("/ping*").handler(healthCheckHandler);

				vertx.createHttpServer().requestHandler(router).listen(8080);
				
				
				vertx.setPeriodic(1000 * 60 * 60 * 6, timer -> { // Refresh keys all 6 hours.
					refreshKeys(authProvider);
				});
			});

			promise.complete();
		});
		return future;
	}

	private Future<Void> createInfra() {
		Future<Void> future = Future.future(promise -> {
			
			// Event Store
			eventStore = EventStoreFactory.getEventStore(config().getString(ConfigProfile.CONFIG_EVENT_STORE, ""), null);
			
			// Read
			HouseRepository ur = new HouseRepository(eventStore);
			// Write
			DispatchEventPublisher houseEventDispatcher = new DispatchEventPublisher();
			houseEventDispatcher.register(new PersistingEventPublisher(eventStore)); // Add persistent handler

			houseHandler = new HouseHandler(houseEventDispatcher, ur);
			promise.complete();
		});
		return future;
	}

	public Future<Void> refreshKeys(JWTAuth authProvider) {
		Future<Void> future = Future.future(promise -> {
			try {
				log.finest("Get authentication public keys.");
				List<PubSecKeyOptions> publicKeys = new ArrayList<PubSecKeyOptions>();
				if (config().containsKey(ConfigProfile.CONFIG_JWT_PUBLICKEYS)) {
					config().getJsonArray(ConfigProfile.CONFIG_JWT_PUBLICKEYS).forEach(key-> {
						log.finest("Add key: " + key);
						publicKeys.add(new PubSecKeyOptions().setAlgorithm("RS256").setPublicKey(String.valueOf(key)));
					});	
				}
				
				GoogleCertificateCrawler.getPublicKeyList().forEach(key -> {
					log.finest("Add key: " + key);
					publicKeys.add(new PubSecKeyOptions().setAlgorithm("RS256").setPublicKey(key));
				});
				
				Boolean useTestingKey = config().getBoolean(ConfigProfile.CONFIG_JWT_TEST_PUBLICKEY);
				if (useTestingKey == null || useTestingKey) {
					publicKeys.add(new PubSecKeyOptions().setAlgorithm("RS256").setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv"
							+ "vkTtwlvBsaJq7S5wA+kzeVOVpVWwkWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHc"
							+ "aT92whREFpLv9cj5lTeJSibyr/Mrm/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIy"
							+ "tvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0"
							+ "e+lf4s4OxQawWD79J9/5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb"
							+ "V6L11BWkpzGXSW4Hv43qa+GSYOD2QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9" + "MwIDAQAB"));
				}
				
				authProvider.rotateKeys(publicKeys);
			} catch (Exception e) {
				e.printStackTrace();
				log.info("Error retrieving authentication public keys. Keep old ones.");
			}
			promise.complete();
		});
		return future;
	}
}
