package com.domosnap.services.restadapter.commandhandler.model;

import com.domosnap.services.domain.user.House;
import com.domosnap.services.infra.repositories.HouseRepository;

import io.vertx.core.json.JsonObject;

public class HouseJsonCodec {
	
	public final static String toJson(House house, HouseRepository houseRepository) {
		if (house == null) {
			return "";
		}
	    StringBuilder sb = new StringBuilder();
	    sb.append("{\"id\":\"").append(house.getId()).append("\",")
	      .append("\"title\":\"").append(house.getTitle()).append("\"}");
//	      .append("\"contactList\":").append(ContactJsonCodec.contactListToJson(user, userRepository)).append("]}");
	
	    return sb.toString();
	}
	
	public final static HouseRest fromJson(String json) {
		if (json == null) {
			return null;
		}
		JsonObject jo = new JsonObject(json);
		HouseRest ur = new HouseRest();
		ur.id = jo.getString("id");
		ur.firstName = jo.getString("firstName");
		ur.lastName = jo.getString("lastName");
		ur.email=jo.getString("email");
		ur.phoneNumber = jo.getString("phoneNumber");
		ur.photoURL= jo.getString("photoURL");
		ur.birthDate = jo.getInstant("birthDate");
		ur.gender = jo.getString("gender");

		return ur;
	}
}
