package com.domosnap.services.restadapter.commandhandler.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.house.Label;

public class LabelJsonCodec {
	
	
	public final static String toJson(List<Label> labels) {
		StringBuilder sb = new StringBuilder("[");
		for (Label label : labels) {
			sb.append(toJson(label)).append(",");
		}
		if (sb.length()!=1) {
			sb.setLength(sb.length()-1);
		}
		sb.append("]");
		return sb.toString();
	}
	
	
	
	public final static String toJson(Label label) {
		if (label == null) {
			return "";
		}
		try {
		    StringBuilder sb = new StringBuilder();
		    sb.append("{\"id\":\"").append(label.getId()).append("\",")
		      .append("\"title\":\"").append(label.getTitle()).append("\",")
		      .append("\"description\":\"").append(label.getDescription()).append("\",")
		      .append("\"controllers\":[");
		    
		    boolean containController = false;
		    for (Controller controller : label.getControllerList()) {
				containController = true;
				sb.append("\"").append("PATHCONTROLLER/").append(URLEncoder.encode(controller.getWhere().getUri(), "UTF-8")).append("\",");
				
			}
		    if (containController) {
		    	sb.setLength(sb.length()-1);
			}
		    sb.append("],")
		      .append("\"labels\":[");
		    boolean containLabel = false;
//		    for (Label subLabel : label.) { // TODO
//		    	containLabel = true;
//				sb.append("\"").append("PATHCONTROLLER/").append(URLEncoder.encode(controller.getWhere().getUri(), "UTF-8")).append("\",");
//				
//			}
		    if (containLabel) {
		    	sb.setLength(sb.length()-1);
			}
		    
		    sb.append("]}");
		    return sb.toString();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ""; // TODO
		}
	}
	
}
