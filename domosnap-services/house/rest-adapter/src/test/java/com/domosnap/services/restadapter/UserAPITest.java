package com.domosnap.services.restadapter;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.domosnap.services.restadapter.ServerProfile;
import com.domosnap.services.restadapter.commandhandler.model.HouseRest;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import io.vertx.core.json.JsonObject;

public class UserAPITest {

	private String host = "localhost";
	private int port = 8080;
	//private String jwt ="eyJhbGciOiJSUzI1NiIsImtpZCI6ImFiZGY2YzhiY2NiNDk4NzAzZGZjOWRmYjgwNGI0ZTE2YmIyNDI1MzQiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYxNDk2NTQxLCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjE0OTY1NDEsImV4cCI6MTU2MTUwMDE0MSwiZW1haWwiOiJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7Imdvb2dsZS5jb20iOlsiMTE1Mjc0NTI4ODU2NDY0NjQ0MDUyIl0sImVtYWlsIjpbIm9saXZpZXIuZHJpZXNiYWNoQGN1cmFudGkuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.UurHAtR1EV-CR7YMpbhGmS0akgHCh4nIgV3cMQlo8GbnKBoQEKLW3gBx_PiFvxTghVRfmoKeyBw9lGt-0NZZ4ylYFyXBRWHeJYsThugmVuPQdDP2Ec4_FLcK15kGS6z2WYgK9ZFyVakQXrjUrMHe727NbN9LaaqVSzq3Il51gm1196W_fzgP22k5Lnadc53-Ghaeqg0cTjabKBrHFCmgg9cKfFmFgYtSdBohSCzozeoKolnHrCfZGBjjU2-vs_orDeyDvzElHli7Cyw7f5mjStB2te7fyD928wia4srZ9MAHJdKXlttJeKgBbn5LlSWJFdu88dX8U2qV4Jo96NtXdw";
	// Use a test public key:
	private String jwt ="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYyMTUyMzU2LCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjIxNTIzNTYsImVtYWlsIjoib2xpdmllci5kcmllc2JhY2hAY3VyYW50aS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjExNTI3NDUyODg1NjQ2NDY0NDA1MiJdLCJlbWFpbCI6WyJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.OLgwhtHzjhVmJV9U_lVCWkyPlRem5tyK8BIdRw2y7nDQzAkoh9sj2O_7uND7D6qSSgW9FQyEpXSdHfXE-YVYO6yHR_IJAt7ATawVtl4krVuKevDGOC6aVHncbfvYENvs-ADCWKYQ6-teEwC99S7_1iwcD5okuH4BQ2aOQe4XW7-7qVdhDHw0Q3BzPe9wlEoVqS1vebmxXBW-Rgt4Z-1LcMzYKVm5YM2Uiw-vibxAFRD1QbM1pm-3uQYlFrpHy5ksiuh4YoaIeZKiJKZNOEOzJ-XmdtvYwIWmtjZ5gI2iol75kQNJ00A2aF7H2pN90v6DTTxvaAT67-Xoc8stYx1XUA";
	
	
	
	@BeforeClass
	public static void initialize() throws Exception {
		// Start server
		ServerProfile.main(new String[] {});
		Thread.sleep(2000);
		// Add json serializer
		JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
		sf.setProvider(new JacksonJaxbJsonProvider());
	}
	
	
	@AfterClass
	public static void stop() {
		Runner.stop();
	}
	
	@Test
	public void testConnection() {

		// Test connection without token
		Client client = ClientBuilder.newClient();

		Response response = client.target("http://" + host + ":" + port)
				.path("/")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(new HouseRest(), MediaType.APPLICATION_JSON));
		
		Assert.assertEquals(404, response.getStatus()); 

	}
	
	@Test
	public void testGet() throws IOException {

		Client client = ClientBuilder.newClient();

		// With wrong Id
		Response response = client.target("http://" + host + ":" + port)
				.path("/users/id")
				.request(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + jwt)
				.get();
		
		Assert.assertEquals(404, response.getStatus());
		
		
		response = client.target("http://" + host + ":" + port)
				.path("/users")
				.request(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + jwt)
				.post(Entity.entity(new HouseRest(), MediaType.APPLICATION_JSON));
		
		assertEquals(201, response.getStatus());
		BufferedReader br = new BufferedReader(new InputStreamReader(((InputStream)response.getEntity())));
        
        String line = br.readLine();
        String id = new JsonObject(line).getString("id");
		
		String result = client.target("http://" + host + ":" + port)
				.path("/users/" + id)
				.request(MediaType.APPLICATION_JSON)
				.header("Authorization", "Bearer " + jwt)
				.get(String.class);
		
		assertEquals(id, new JsonObject(result).getString("id"));

		response = client.target("http://" + host + ":" + port)
				.path("/users/" + id)
				.request()
				.header("Authorization", "Bearer " + jwt)
				.delete();
		
		Assert.assertEquals(204, response.getStatus());
		
		// TODO add test to get all properties.
	}
	
}