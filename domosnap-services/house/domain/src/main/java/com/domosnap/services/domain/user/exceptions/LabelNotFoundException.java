package com.domosnap.services.domain.user.exceptions;

public class LabelNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public LabelNotFoundException(String labelId) {
		super("Label [".concat(labelId).concat("] not found."));
	}
}
