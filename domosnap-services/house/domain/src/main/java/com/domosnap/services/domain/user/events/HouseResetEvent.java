package com.domosnap.services.domain.user.events;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.services.domain.user.HouseId;

public class HouseResetEvent extends EventBase<HouseId> {

    public HouseResetEvent(HouseId userId, ResourceName creator) {
    	super(userId, creator);
    }
}
