package com.domosnap.services.domain.user.events.label;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.services.domain.user.HouseId;

public class LabelAddedEvent extends EventBase<HouseId> {

	private final String title;
	private final String description;
	private final String labelId;

    public LabelAddedEvent(HouseId houseId, String labelId, String title, String description, ResourceName creator) {
    	super(houseId, creator);
    	this.labelId = labelId;
    	this.description = description;
        this.title = title;
    }
    
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getLabelId() {
		return labelId;
	}
}
