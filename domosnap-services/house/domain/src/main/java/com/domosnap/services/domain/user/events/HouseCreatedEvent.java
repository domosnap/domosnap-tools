package com.domosnap.services.domain.user.events;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.services.domain.user.HouseId;

public class HouseCreatedEvent extends EventBase<HouseId> {

	private final String title;

    public HouseCreatedEvent(HouseId userId, String title, ResourceName creator) {
    	super(userId, creator);
        this.title = title;
    }
    
	public String getTitle() {
		return title;
	}
}
