package com.domosnap.services.domain.user;

import java.util.UUID;

import com.domosnap.tools.core.services.cqrs.domain.AggregateId;

public class HouseId implements AggregateId {
    private String value;

    public HouseId(String value) {

        this.value = value;
    }

    public static HouseId generate() {
        return new HouseId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HouseId messageId = (HouseId) o;

        return !(value != null ? !value.equals(messageId.value) : messageId.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value;
    }

	@Override
	public String getId() {
		return value;
	}
}
