package com.domosnap.services.domain.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Logger;

import com.domosnap.engine.house.Label;
import com.domosnap.services.domain.user.events.HouseChangeEvent;
import com.domosnap.services.domain.user.events.HouseCreatedEvent;
import com.domosnap.services.domain.user.events.HouseResetEvent;
import com.domosnap.services.domain.user.events.label.LabelAddedEvent;
import com.domosnap.services.domain.user.events.label.LabelRemovedEvent;
import com.domosnap.services.domain.user.exceptions.LabelAlreadyExistingException;
import com.domosnap.services.domain.user.exceptions.LabelNotFoundException;
import com.domosnap.tools.core.services.cqrs.domain.Aggregate;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.impl.ProjectionBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class House implements Aggregate<HouseId> {

	private Logger log = Logger.getLogger(House.class.getName());

	private HouseUserProjection projection;

	public static void create(HouseId userId, String title, ResourceName principal, Consumer<Event<?>> eventPublisher) {
		eventPublisher.accept(new HouseCreatedEvent(userId, title, principal));
	}

	public House(List<Event<?>> history) {
		projection = new HouseUserProjection(history);
	}

	public void changeHouseProperties(String title, ResourceName principal, Consumer<Event<?>> eventPublisher) {
		Event<HouseId> event = new HouseChangeEvent(getId(), title, principal);
		projection.apply(event);
		eventPublisher.accept(event);
	}

	public Label addLabel(String labelId, String title, String description, ResourceName principal,
			Consumer<Event<?>> eventPublisher) throws LabelAlreadyExistingException {
		if (labelId != null && getLabel(labelId) != null) {
			throw new LabelAlreadyExistingException(labelId);
		} else {
			if (labelId == null) {
				labelId = UUID.randomUUID().toString();
			}
			Event<HouseId> event = new LabelAddedEvent(getId(), labelId, title, description, principal);
			projection.apply(event);
			eventPublisher.accept(event);
			return getLabel(labelId);
		}
	}

	public void removeLabel(String labelId, ResourceName principal, Consumer<Event<?>> eventPublisher) throws LabelNotFoundException {
		if (labelId != null && getLabel(labelId) != null) {
			throw new LabelNotFoundException(labelId);
		} else {
			Event<HouseId> event = new LabelRemovedEvent(getId(), labelId, principal);
			projection.apply(event);
			eventPublisher.accept(event);
		}
	}

	public void reset(ResourceName principal, Consumer<Event<?>> eventPublisher) {
		Event<HouseId> event = new HouseResetEvent(getId(), principal);
		projection.apply(event);
		eventPublisher.accept(event);
	}

	// QUERY
	public HouseId getId() {
		return projection.id;
	}

	public String getTitle() {
		return projection.title;
	}

	public Label getLabel(String id) {
		for (Label label : projection.house.getLabels()) {
			if (label.getId().equals(id)) {
				return label;
			}
		}
		return null;
	}

	public List<Label> getLabels() {
		return Collections.unmodifiableList(projection.house.getLabels());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"id\":\"").append(getId()).append("\",").append("\"title\":\"").append(getTitle()).append("\"}");
//          .append("\"contactList\":{").append(getContactList()).append("}}");

		return sb.toString();
	}

	class HouseUserProjection extends ProjectionBase {
		public HouseId id;
		public com.domosnap.engine.house.House house = new com.domosnap.engine.house.House();
		public String title;
		public boolean deleted = false;
		public List<Event<?>> history = new ArrayList<>();

		public HouseUserProjection(List<Event<?>> history) {
			register(HouseCreatedEvent.class, this::apply);
			register(HouseChangeEvent.class, this::apply);
			register(HouseResetEvent.class, this::apply);
			register(LabelAddedEvent.class, this::apply);
			register(LabelRemovedEvent.class, this::apply);
			history.forEach(this::apply);
		}

		public void apply(HouseCreatedEvent event) {
			this.id = event.getAggregateId();
			this.title = event.getTitle();
			history.add(event);
		}

		public void apply(HouseChangeEvent event) {
			if (event.getTitle() != null) {
				this.title = event.getTitle();
			}
			history.add(event);
		}

		public void apply(HouseResetEvent event) {
			house.getLabels().clear();
			title = "";
			history.add(event);
		}
		
		public void apply(LabelAddedEvent event) {
			Label label = new Label();
			label.setTitle(event.getTitle());
			label.setDescription(event.getDescription());
			house.getLabels().add(label);
			history.add(event);
		}
		
		public void apply(LabelRemovedEvent event) {
			for (Label label : house.getLabels()) {
				if (label.getId() == event.getLabelId()) {
					house.getLabels().remove(label);
					break;
				}
			}
			history.add(event);
		}
	}
}
