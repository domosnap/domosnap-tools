package com.domosnap.services.domain.user.events.label;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;
import com.domosnap.services.domain.user.HouseId;

public class LabelRemovedEvent extends EventBase<HouseId> {

	private final String labelId;

    public LabelRemovedEvent(HouseId houseId, String labelId, ResourceName creator) {
    	super(houseId, creator);
    	this.labelId = labelId;
    }
    
	public String getLabelId() {
		return labelId;
	}
}
