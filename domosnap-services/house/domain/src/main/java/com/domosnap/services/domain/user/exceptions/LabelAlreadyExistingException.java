package com.domosnap.services.domain.user.exceptions;

public class LabelAlreadyExistingException extends Exception {

	private static final long serialVersionUID = 1L;

	public LabelAlreadyExistingException(String labelId) {
		super("Label [".concat(labelId).concat("] is already existing."));
	}
}
