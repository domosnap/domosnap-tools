package com.domosnap.services.domain.user.events;

import com.domosnap.services.domain.user.HouseId;
import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class HouseChangeEvent extends EventBase<HouseId> {

	private final String title;

	public HouseChangeEvent(HouseId userId, String title, ResourceName creator) {
		super(userId, creator);
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
}
