# curanti-user-management



*   maven 3
*   OpenJDK 8 (raspberry constraint)



*   [domain](https://gitlab.com/domosnap/domosnap-services/house/tree/master/domain "domain"): core business implementation about user domain.
*   [infra](https://gitlab.com/domosnap/domosnap-services/house/tree/master/infra "infra"): technical implementation (eventStore, repository, etc..)
*   [rest-adapter](https://gitlab.com/domosnap/domosnap-services/house/tree/master/rest-adapter "rest-adapter")rest-adapter: external Adapter to communicate with the core busines. In other word: REST API.



	mvn clean package
	cd rest-adapter/target
	java -jar rest-adapter-0.x.x-SNAPSHOT.jar -conf ../src/main/configuration/default-configuration.json


    docker login
    docker run registry.gitlab.com/domosnap/domosnap-services/house:[tag]


    mvn clean package
    sudo docker build -f Dockerfile_arm32v6 . -t adegiuli/house-arm32v6:latest
    sudo docker push adegiuli/house-arm32v6:latest
    kubectl delete configmap house
    kubectl create configmap house --from-file=config.json=./rest-adapter/src/main/configuration/test-configuration.json
    kubectl apply -f house-arm32v6.yaml
    


On pipeline just lunch the deploy job.

Remark: For new project don't forget to register the token:
    - Create key in gitlab (Settings->Repository->Deploy Tokens)
    - On k8s cluster add the key:

    kubectl create secret docker-registry regsecret-house --docker-server=registry.gitlab.com --docker-username='gitlab-ci-token' --docker-password='generated-token'

Don't forget to use imagePullSecret in pod definition: 
    
    imagePullSecrets:
        - name: regsecret-house


[API Documentation](https://app.swaggerhub.com/apis/arnauddegiuli/house/1.0.0#/ "API Documentation") 

openssl x509 -pubkey -noout -in <certificat.pem> > <fichier de sortie.pub>

&copy;  domosnap.com
