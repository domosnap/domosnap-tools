package com.domosnap.services.infra.handlers.metric;

import java.util.logging.Logger;

import com.domosnap.services.domain.user.events.HouseChangeEvent;
import com.domosnap.services.domain.user.events.HouseResetEvent;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.handler.SingleEventPublisher;

public class ExampleHandler extends SingleEventPublisher {

	private Logger log = Logger.getLogger(ExampleHandler.class.getName());
	
	protected <T extends Event<?>> void register(Class<T> eventClass) {
		register(eventClass, this::apply);
	}

	public void apply(Event<?> event) {
		try {
//			mixpanel.sendMessage(
//					messageBuilder.event(event.getCreator().getAggregateId(), event.getClass().getSimpleName(), new JSONObject(event)));
		
		} catch (Exception e) {
			log.severe("Metric error: impossible to send event. Details: ".concat(e.getMessage()));
		}
	}

	public ExampleHandler(String projectToken) {
		register(HouseChangeEvent.class);
		register(HouseResetEvent.class);
	}

}
