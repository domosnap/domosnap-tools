package com.domosnap.services.infra.repositories;


import java.util.List;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.domosnap.tools.core.services.cqrs.infra.repository.AggregateRepository;
import com.domosnap.services.domain.user.House;


public class HouseRepository extends AggregateRepository<House> {

    public HouseRepository(EventStore eventStore) {
        super(eventStore);
    }

    @Override
    protected House fromHistory(List<Event<?>> history) {
        return new House(history);
    }
}
