package com.domosnap.tools.core.services.eventstore;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;

/*
 * #%L
 * DomoSnap Event to Kafka Consumer
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventCodec;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;

public class KafkaEventStore implements EventStore {

	private static Logger log = Logger.getLogger(KafkaEventStore.class.getSimpleName());

	public static final String TOPIC_CONFIG = "kafka.topic";
	private String bootstrap;
	private String topic;
	private Map<String, List<Event<?>>> eventsCache = new HashMap<>();
	private KafkaProducer<String, String> producer;
	private KafkaConsumer<String, String> consumer;

	public KafkaEventStore() {
	}

	private synchronized List<Event<?>> fromPersistence(String aggregateId) {
		List<Event<?>> result = new ArrayList<>();

		if (!consumer.listTopics().containsKey(topic)) {
			return result;  // TODO clean up code
		}

		final List<PartitionInfo> partitionInfos = consumer.partitionsFor(topic);
		final Collection<TopicPartition> topicPartitions = new ArrayList<>();

		for (final PartitionInfo partitionInfo: partitionInfos) {
			topicPartitions.add(new TopicPartition(partitionInfo.topic(), partitionInfo.partition()));
		}

		consumer.assign(topicPartitions);
		consumer.seekToBeginning(topicPartitions);
		ConsumerRecords<String, String> records = consumer.poll(1000);

		for (ConsumerRecord<String, String> record : records) {
			Event<?> event = EventCodec.fromJson(record.value());
			// Ne garder que les événements pour cet aggregateId
			if (event.getAggregateId().getId().equals(aggregateId)) {
				result.add(event);
			}
		}

		return result;
	}

	@Override
	public List<Event<?>> getEventsOfAggregate(String aggregateId) {
		return eventsCache.computeIfAbsent(aggregateId, this::fromPersistence);
	}

	@Override
	public void store(Event<?> event) {
		ProducerRecord<String, String> record = new ProducerRecord<>(topic,
				event.getAggregateId().getId(),
				EventCodec.toJson(event));

		producer.send(record, (metadata, exception) -> {
			if (exception != null) {
				log.severe(exception.getMessage());
			} else {
				log.finest("Event sent to Kafka successfully [" + event + "]");
				eventsCache.remove(event.getAggregateId().getId());
			}
		});
		producer.flush();
	}

	@Override
	public void close() {
		producer.close();
		consumer.close();
	}

	@Override
	public void init(Map<String, Object> properties) {
		Properties producerProps = new Properties();

		bootstrap = (String) properties.get(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG);
		if (bootstrap == null) {
			throw new IllegalArgumentException("Bootstrap servers must be configured");
		}
		producerProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap);

		topic = (String) properties.get(TOPIC_CONFIG);
		if (topic == null) {
			throw new IllegalArgumentException("Topic must be configured");
		}

		producerProps.put(ProducerConfig.ACKS_CONFIG, "all");
		producerProps.put(ProducerConfig.RETRIES_CONFIG, "0");
		producerProps.put(ProducerConfig.BATCH_SIZE_CONFIG, "1");
		producerProps.put(ProducerConfig.LINGER_MS_CONFIG, "1");
		producerProps.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
		producerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		producerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");

		producer = new KafkaProducer<>(producerProps);

		Properties consumerProps = new Properties();
		consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap);
		consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, properties.get(ConsumerConfig.GROUP_ID_CONFIG).toString());
		consumerProps.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
		consumerProps.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
		consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringDeserializer");
		consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringDeserializer");

		consumer = new KafkaConsumer<>(consumerProps);


	}
}
