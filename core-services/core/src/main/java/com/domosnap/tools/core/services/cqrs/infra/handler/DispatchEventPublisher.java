package com.domosnap.tools.core.services.cqrs.infra.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;

public class DispatchEventPublisher implements EventPublisher {
    private Map<Class<? extends Event<?>>, List<Consumer<Event<?>>>> allHandlers = new HashMap<>();
    private List<Consumer<Event<?>>> allEventHandlers = new ArrayList<>();
    
    public void register(Class<? extends Event<?>> eventClass, Consumer<Event<?>> handler) {
        List<Consumer<Event<?>>> handlers = allHandlers.getOrDefault(eventClass, emptySet());
        handlers.add(handler);
        allHandlers.put(eventClass, handlers);
    }

    public void register(Consumer<Event<?>> handler) {
        allEventHandlers.add(handler);
    }

    public void unregister(Class<? extends Event<?>> eventClass, Consumer<Event<?>> handler) {
    	List<Consumer<Event<?>>> handlers = allHandlers.get(eventClass);
    	if (handlers != null) {
    		handlers.remove(handler);
    	}
    }

    public void unregister(Consumer<Event<?>> handler) {
    	allEventHandlers.remove(handler);
    }

    @Override
    public void accept(Event<?> event) {
        allEventHandlers.forEach(handler -> handler.accept(event)); // TODO put in first place since persistenceHanlder is often here... need to manage an order in register...
        List<Consumer<Event<?>>> handlers = allHandlers.getOrDefault(event.getClass(), emptySet());
        handlers.forEach(handler -> handler.accept(event));

    }

    private List<Consumer<Event<?>>> emptySet() {
        return new ArrayList<>();
    }
    

}