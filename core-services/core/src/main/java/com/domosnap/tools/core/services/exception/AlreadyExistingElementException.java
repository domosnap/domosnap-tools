package com.domosnap.tools.core.services.exception;

public class AlreadyExistingElementException extends RuntimeException {
	    private static final long serialVersionUID = 6769829250639411880L;

	    /**
	     * Constructs a <code>AlreadyExistingException</code> with <tt>null</tt>
	     * as its error message string.
	     */
	    public AlreadyExistingElementException() {
	        super();
	    }

	    /**
	     * Constructs a <code>AlreadyExistingException</code>, saving a reference
	     * to the error message string <tt>s</tt> for later retrieval by the
	     * <tt>getMessage</tt> method.
	     *
	     * @param   s   the detail message.
	     */
	    public AlreadyExistingElementException(String s) {
	        super(s);
	    }
	}

