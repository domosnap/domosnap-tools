package com.domosnap.tools.core.services.cqrs.infra.eventStore;

import org.json.JSONException;
import org.json.JSONObject;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import static java.lang.Thread.currentThread;

public class EventCodec {
	// TODO revoir serialization of event... of aggreggate, of ........
	public static Event<?> fromJson(String json) {

		try {
			JSONObject jo = new JSONObject(json);
			String type = jo.getString("className");

			return (Event<?>) new Gson().fromJson(json, currentThread().getContextClassLoader().loadClass(type));
		} catch (JsonSyntaxException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String toJson(Event<?> event) {
		return new Gson().toJson(event).toString();
	}
}
