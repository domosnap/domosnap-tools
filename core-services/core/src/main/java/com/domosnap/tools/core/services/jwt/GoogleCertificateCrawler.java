package com.domosnap.tools.core.services.jwt;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import org.json.JSONObject;

public class GoogleCertificateCrawler {

	private static Logger log = Logger.getLogger(GoogleCertificateCrawler.class.getName());
	
	public static void main(String[] args) {
		
		try {
			getPublicKeyList().forEach(publicKey -> {
				System.out.println("Public key: " + publicKey);
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static List<String> getPublicKeyList() throws IOException {
		List<String> result = new ArrayList<>();
		// TODO utiliser le timeout de la page
		try {
			String json = readStringFromURL("https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com");
			log.finest("Get google certificate: [".concat(json).concat("]."));
			
			JSONObject jo = new JSONObject(json);
			
			
			jo.keys().forEachRemaining(key -> {
				try {
					String certificateString = jo.getString(key);
	
					InputStream fin = new ByteArrayInputStream(certificateString.getBytes());
					CertificateFactory f = CertificateFactory.getInstance("X.509");
					X509Certificate certificate = (X509Certificate)f.generateCertificate(fin);
					PublicKey pk = certificate.getPublicKey();
					
					 byte[] pubByte = pk.getEncoded();
					 String pubKeyStr = new String(Base64.getEncoder().encode(pubByte));
					 result.add(pubKeyStr);
					
				} catch (CertificateException e) {
					log.severe("Error to decode certificate: ".concat(e.getMessage()));
				}
			});
		} catch (IOException e) {
			log.severe("Error getting certificate: ".concat(e.getMessage()));
			throw e;
		}
		return result;
	}
	
	public static String readStringFromURL(String requestURL) throws IOException
	{
	    try (Scanner scanner = new Scanner(new URL(requestURL).openStream(),
	            StandardCharsets.UTF_8.toString()))
	    {
	        scanner.useDelimiter("\\A");
	        return scanner.hasNext() ? scanner.next() : "";
	    }
	}
}
