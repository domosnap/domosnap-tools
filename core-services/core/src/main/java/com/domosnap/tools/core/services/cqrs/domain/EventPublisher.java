package com.domosnap.tools.core.services.cqrs.domain;

import java.util.function.Consumer;

public interface EventPublisher extends Consumer<Event<?>> {
}
