package com.domosnap.tools.core.services.cqrs.domain.impl;

import java.util.UUID;

import com.domosnap.tools.core.services.cqrs.domain.AggregateId;
import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

/*
 * 
 * Only getter are serialized => use them if you want your properties/values be serialized!
 */
public abstract class EventBase<T extends AggregateId> implements Event<T> {

	private final UUID uuid;
	private final T aggregateId;
	private final ResourceName creator;
	private final String className;

	public EventBase(T aggregateId, ResourceName creator) {
		this.uuid = UUID.randomUUID();
		this.aggregateId = aggregateId;
		this.className = this.getClass().getName();
		this.creator = creator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventBase<?> other = (EventBase<?>) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	
	@Override
	public UUID getUuId() {
		return uuid;
	}

	@Override
	public T getAggregateId() {
		return aggregateId;
	}
	
	@Override
	public ResourceName getCreator() {
		return creator;
	}
	
	@Override
	public String getClassName() {
		return className;
	}
}
