package com.domosnap.tools.core.services.resourcenames;

/**
 * Represent a resource identifier.
 * //domain/path/id <=> //profile/user/e5d894d5-bf93-4b04-a8c7-7e75d2f15af6
 */
// //domain/pathToAggregate/id => //profile/user/e5d894d5-bf93-4b04-a8c7-7e75d2f15af6
public class ResourceName {

	public static final String DOMAIN_PROFILE = "profile";
	public static final String DOMAIN_HOUSE = "house";
	public static final String DOMAIN_SMARTDEVICE = "smartdevice";
	
	
	private String domain;
	private String path;
	private String id;
	
	public ResourceName(String domain, String path, String id) {
		this.domain = domain;
		this.path = path;
		this.id = id;
	}
	
	public ResourceName(String urn) {
		if (!urn.startsWith("//")) {
			throw new IllegalArgumentException("Not well formed urn [" + urn + "]");
		} else {
			urn = urn.substring(2);
		}

		int last = urn.lastIndexOf("/");
		int first = urn.indexOf("/");
		
		if ( first == last) {
			throw new IllegalArgumentException("Not well formed urn [" + urn + "]");
		}
		
		id = urn.substring(last+1);
		if (id.length() == 0) {
			throw new IllegalArgumentException("Not well formed urn [" + urn + "]");
		}

		domain = urn.substring(0, first);
		if (domain.length() == 0) {
			throw new IllegalArgumentException("Not well formed urn [" + urn + "]");
		}
		
		path = urn.substring(first+1, last);
		if (path.length() == 0) {
			throw new IllegalArgumentException("Not well formed urn [" + urn + "]");
		}	
	}
	
	public String getAggregateId() {
		return id;
	}
	
	public String getDomain() {
		return domain;
	}
	
	public String getPath() {
		return path;
	}
	
	public String toString() {
		return "//".concat(domain).concat("/").concat(path).concat("/").concat(id);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domain == null) ? 0 : domain.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResourceName other = (ResourceName) obj;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.equals(other.domain))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}
	
	
	
}
