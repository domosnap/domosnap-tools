package com.domosnap.tools.core.services.cqrs.infra.eventStore;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.domosnap.tools.core.services.cqrs.infra.eventStore.impl.InMemoryEventStore;

public class EventStoreFactory {

	private final static Logger log = Logger.getLogger(EventStoreFactory.class.getName());
	
	public static final EventStore getEventStore(String eventStoreClass, Map<String, Object> config) {
		try {
			if (eventStoreClass == null || eventStoreClass.isEmpty()) {
				return new InMemoryEventStore();
			} else {
				EventStore eventStore = (EventStore) Class.forName(eventStoreClass).getDeclaredConstructor().newInstance();
				if(log.isLoggable(Level.FINEST)) {
					log.finest("EventStore init config:");
					config.forEach((k, v) -> {log.finest("Key [".concat(k).concat("] => Value [").concat(String.valueOf(v)));});
				}
				eventStore.init(config);
				return eventStore;
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unsupported event store [" + eventStoreClass + "].");
		}
	}
	
}
