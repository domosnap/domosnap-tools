package com.domosnap.tools.core.services.cqrs.infra.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;

public class SingleEventPublisher implements EventPublisher {

	private Map<Class<?>, Consumer<Event<?>>> appliers = new HashMap<>();

	@SuppressWarnings("unchecked")
	public <T extends Event<?>> void register(Class<T> eventClass, Consumer<T> eventConsumer) {
		appliers.put(eventClass, (Consumer<Event<?>>) eventConsumer);
	}
	
	@Override
	public void accept(Event<?> event) {
		Consumer<Event<?>> consumer = appliers.get(event.getClass());
		if (consumer != null) {
			consumer.accept(event);
		}
	}
}
