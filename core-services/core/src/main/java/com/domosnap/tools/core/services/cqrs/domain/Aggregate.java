package com.domosnap.tools.core.services.cqrs.domain;

@ com.domosnap.tools.core.services.cqrs.doc.Aggregate
public interface Aggregate<T extends AggregateId> {

	public T getId();

//	public ResourceName getResourceName();
}
