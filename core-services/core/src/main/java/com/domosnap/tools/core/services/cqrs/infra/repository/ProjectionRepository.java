package com.domosnap.tools.core.services.cqrs.infra.repository;

public abstract class ProjectionRepository<T> {

    public abstract T save(T message);
    public abstract T getById(String projectionId);
}
