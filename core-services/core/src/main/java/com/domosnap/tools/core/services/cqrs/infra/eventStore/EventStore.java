package com.domosnap.tools.core.services.cqrs.infra.eventStore;


import java.util.List;
import java.util.Map;

import com.domosnap.tools.core.services.cqrs.domain.Event;

public interface EventStore {
    List<Event<?>> getEventsOfAggregate(String aggregateId);
    void store(Event<?> event);

    void init(Map<String, Object> props);
    void close();
}
