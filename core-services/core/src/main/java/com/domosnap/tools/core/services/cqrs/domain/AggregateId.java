package com.domosnap.tools.core.services.cqrs.domain;

public interface AggregateId {
	public String getId();
}
