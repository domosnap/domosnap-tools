package com.domosnap.tools.core.services.crypto;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class CryptoTools {

	public String encrypt(String message, String key) {
		try {
			Key clef = new SecretKeySpec(key.getBytes("ISO-8859-2"), "Blowfish");
			Cipher cipher = Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE, clef);
			return new String(cipher.doFinal(message.getBytes()));
		} catch (Exception e) {
			return null;
		}
	}

	public String decrypt(String message, String key) {
		try {
			Key clef = new SecretKeySpec(key.getBytes("ISO-8859-2"), "Blowfish");
			Cipher cipher = Cipher.getInstance("Blowfish");
			cipher.init(Cipher.DECRYPT_MODE, clef);
			return new String(cipher.doFinal(message.getBytes()));
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
}
