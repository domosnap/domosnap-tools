package com.domosnap.tools.core.services.cqrs.infra.eventStore.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;

public class InMemoryEventStore implements EventStore {
    private Map<String, List<Event<?>>> events = new HashMap<>();

    private List<Event<?>> emptyList() {
        return new ArrayList<>();
    }

    @Override
    public List<Event<?>> getEventsOfAggregate(String aggregateId) {
        return events.getOrDefault(aggregateId, emptyList());
    }

    @Override
    public void store(Event<?> event) {
   		List<Event<?>> aggregateEvents = events.getOrDefault(event.getAggregateId().getId(), emptyList());
   		aggregateEvents.add(event);
   		events.put(event.getAggregateId().getId(), aggregateEvents);
    }

	@Override
	public void close() {
	}

	@Override
	public void init(Map<String, Object> props) {
	}
}
