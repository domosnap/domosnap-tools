package com.domosnap.tools.core.services.cqrs.domain.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;

import com.domosnap.tools.core.services.cqrs.doc.Projection;
import com.domosnap.tools.core.services.cqrs.domain.Event;

@Projection
public abstract class ProjectionBase implements com.domosnap.tools.core.services.cqrs.domain.Projection {

	private Logger log = Logger.getLogger(ProjectionBase.class.getName());
	
	private Map<Class<? extends Event<?>>, Consumer<? extends Event<?>>> appliers = new HashMap<>();

    public <T extends Event<?>> void register(Class<T> eventClass, Consumer<T> eventConsumer) {
        appliers.put(eventClass, eventConsumer);
    }

    @SuppressWarnings("unchecked")
    public <T extends Event<?>> void apply(T event) {
    	try {
        Consumer<T> consumer = (Consumer<T>) appliers.get(event.getClass());
        consumer.accept(event); 
    	} catch (NullPointerException e) {
    		log.warning("Event not register in Projection: " + event.getClass().getName());
    		// TODO logger la stacktaace car la je catch des nullpooint = dans le cas d'une nullpointerremonter par autre cose..... = ou filtrer en fonction d'ou vient le null pointer pour ne faire cela que dans le cas ou c non registered => sinon thorw l'exsception!
    		throw e;
		}
    }
}
