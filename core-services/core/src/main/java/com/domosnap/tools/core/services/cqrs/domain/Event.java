package com.domosnap.tools.core.services.cqrs.domain;

import java.util.UUID;

import com.domosnap.tools.core.services.resourcenames.ResourceName;

public interface Event<T extends AggregateId> {
   
	UUID getUuId();
	T getAggregateId();
	String getClassName();
	ResourceName getCreator();
	
}
