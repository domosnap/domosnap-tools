package com.domosnap.tools.core.services.cqrs.infra.handler;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.domain.EventPublisher;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;

public class PersistingEventPublisher implements EventPublisher {
    private final EventStore store;

    public PersistingEventPublisher(EventStore store) {
        this.store = store;
    }

	@Override
	public void accept(Event<?> event) {
		store.store(event);
	}
}
