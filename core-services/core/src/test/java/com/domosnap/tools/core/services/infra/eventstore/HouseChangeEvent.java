package com.domosnap.tools.core.services.infra.eventstore;


import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class HouseChangeEvent extends EventBase<HouseId> {

	private final String description;
	private final String name;

	public HouseChangeEvent(HouseId userId, String description, String name, ResourceName creator) {
		super(userId, creator);
		this.description = description;
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}
}
