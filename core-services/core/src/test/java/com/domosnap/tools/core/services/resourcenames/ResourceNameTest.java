package com.domosnap.tools.core.services.resourcenames;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class ResourceNameTest {

	
	@Test
	public void parse() {
		String urn1 = "domain";
		
		try {
			new ResourceName(urn1);
			Assert.fail("Urn not well formed: should throw an exception!");
		} catch (IllegalArgumentException e) {
			// Good!
		} catch (Exception e) {
			Assert.fail("Why this exception?");
		}
		
		
		String urn2 = "//domain";
		try {
			new ResourceName(urn2);
			Assert.fail("Urn not well formed: should throw an exception!");
		} catch (IllegalArgumentException e) {
			// Good!
		} catch (Exception e) {
			Assert.fail("Why this exception?");
		}
		
		
		String urn3 = "//domain//test";
		try {
			new ResourceName(urn3);
			Assert.fail("Urn not well formed: should throw an exception!");
		} catch (IllegalArgumentException e) {
			// Good!
		} catch (Exception e) {
			Assert.fail("Why this exception?");
		}
		
		String urn4 = "//domain/path";
		try {
			new ResourceName(urn4);
			Assert.fail("Urn not well formed: should throw an exception!");
		} catch (IllegalArgumentException e) {
			// Good!
		} catch (Exception e) {
			Assert.fail("Why this exception?");
		}
		
		String urn5 = "//domain/path/id";
		ResourceName rn = new ResourceName(urn5);
		Assert.assertEquals("domain", rn.getDomain());
		Assert.assertEquals("path", rn.getPath());
		Assert.assertEquals("id", rn.getAggregateId());
		
		String urn6 = "//domain/path/aggregate/id";
		rn = new ResourceName(urn6);
		Assert.assertEquals("domain", rn.getDomain());
		Assert.assertEquals("path/aggregate", rn.getPath());
		Assert.assertEquals("id", rn.getAggregateId());
		
		String urn7 = "//domain/aggregate/id";
		rn = new ResourceName(urn7);
		Assert.assertEquals("domain", rn.getDomain());
		Assert.assertEquals("aggregate", rn.getPath());
		Assert.assertEquals("id", rn.getAggregateId());
		
		String urn8 = "//profile/path/user/e5d894d5-bf93-4b04-a8c7-7e75d2f15af6";
		rn = new ResourceName(urn8);
		Assert.assertEquals("profile", rn.getDomain());
		Assert.assertEquals("path/user", rn.getPath());
		Assert.assertEquals("e5d894d5-bf93-4b04-a8c7-7e75d2f15af6", rn.getAggregateId());
		
		String urn9 = "//profile/user/e5d894d5-bf93-4b04-a8c7-7e75d2f15af6";
		rn = new ResourceName(urn9);
		Assert.assertEquals("profile", rn.getDomain());
		Assert.assertEquals("user", rn.getPath());
		Assert.assertEquals("e5d894d5-bf93-4b04-a8c7-7e75d2f15af6", rn.getAggregateId());
	}
}
