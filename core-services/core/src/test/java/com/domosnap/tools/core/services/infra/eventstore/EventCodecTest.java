package com.domosnap.tools.core.services.infra.eventstore;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventCodec;
import com.domosnap.tools.core.services.resourcenames.ResourceName;



public class EventCodecTest {

	@Test
	public void testConvertion() {
		
		HouseId userId = HouseId.generate();
		String description = "description";
		String name = "name";
		ResourceName creator = new ResourceName("//" + ResourceName.DOMAIN_HOUSE + "/users/" + userId.getId());
		
		HouseChangeEvent event = new HouseChangeEvent(userId, description, name, creator);
		String json = EventCodec.toJson(event);
		
		Event<?> eventFromJson = EventCodec.fromJson(json);
		
		Assert.assertEquals(userId, eventFromJson.getAggregateId());
		Assert.assertEquals(description, ((HouseChangeEvent) eventFromJson).getDescription());
		Assert.assertEquals(name, ((HouseChangeEvent) eventFromJson).getName());
		Assert.assertEquals(event.getUuId(), ((HouseChangeEvent) eventFromJson).getUuId());
		Assert.assertEquals(event.getCreator(), ((HouseChangeEvent) eventFromJson).getCreator());
	}
}
