package com.domosnap.tools.core.services.eventstore;

import java.util.ArrayList;

/*
 * #%L
 * DomoSnap Event to Kafka Consumer
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventCodec;
import com.domosnap.tools.core.services.cqrs.infra.eventStore.EventStore;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBEventStore implements EventStore {

	public static String MONGO_URL = "mongo_url";
	public static String AGGREGATE_TYPE = "aggregate_type";

	private static Logger log = Logger.getLogger(MongoDBEventStore.class.getSimpleName());

	private Map<String, List<Event<?>>> events = new HashMap<>();
	private MongoClient mongoClient;
	private MongoDatabase database;
	
	
	public MongoDBEventStore() {
	}

	private List<Event<?>> fromPersistence(String aggregateId) {
		List<Event<?>> result = new ArrayList<Event<?>>();
		
		
		try {
			MongoCollection<Document> collection = database.getCollection(aggregateId); // collection = instance
			
			FindIterable<Document> results = collection.find();
			
			for (Document record : results) {
				if (Level.FINEST.equals(log.getLevel())) {
					log.finest("Retrieve Event [".concat(record.toJson()).concat("] for aggregate [").concat(aggregateId).concat("]."));
				}
//				System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
				result.add(EventCodec.fromJson(record.toJson())); // TODO improve performance here = object => json => event... could be directly object => event
			}
		}catch (IllegalArgumentException e) {
			// TODO: handle exception
			// doesn't exist return null?
			return result; // TODO clean up code
		}

		return result;

	}

	@Override
	public List<Event<?>> getEventsOfAggregate(String aggregateId) {
		return events.getOrDefault(aggregateId, fromPersistence(aggregateId));
	}

	@Override
	public void store(Event<?> event) {
		MongoCollection<Document> collection = database.getCollection(event.getAggregateId().getId()); // collection = instance
		Document person = Document.parse(EventCodec.toJson(event)).append("_id", event.getUuId().toString());  // TODO improve here => transform to json to parse .... maybe directory transform to DBOject
		collection.insertOne(person);
	}

	@Override
	public void close() {
		mongoClient.close();
	}

	@Override
	public void init(Map<String, Object> properties) {
		String databaseName = String.valueOf(properties.getOrDefault(AGGREGATE_TYPE, "default_aggregate"));
		String url = String.valueOf(properties.getOrDefault(MONGO_URL, "mongodb://localhost:27017"));
		mongoClient = new MongoClient(new MongoClientURI(url));
		database = mongoClient.getDatabase(databaseName); // DB = type!
	}

}
