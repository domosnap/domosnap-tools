package com.domosnap.tools.core.services.eventstore;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongoCmdOptionsBuilder;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;

/**
 * Classe abstraite utilisée pour les tests des DAO 
 * @author ssenouci
 *
 */
public abstract class AbstractMongoDbTest {

	/**
	 * Instance du process mongod
	 */
	private static MongodProcess mongodProcess;
	/**
	 * Instance de l'exécutable
	 */
	private static MongodExecutable mongodExecutable;
	private static final String IP = "127.0.0.1";
	private static final int PORT = 27017;
	private static final String DB_NAME = "blog";

	/**
	 * Initialise l'exécutable et le process mongod avant l'exécution des tests
	 * @throws IOException
	 */
	@BeforeClass
	public static void beforeClass() throws IOException {
		MongodStarter starter = MongodStarter.getDefaultInstance();
		IMongodConfig mongodConfig = new MongodConfigBuilder()
			.version(Version.Main.PRODUCTION)
			.net(new Net(IP, PORT, false))
			.cmdOptions(new MongoCmdOptionsBuilder()
			.useSmallFiles(true)
			.useNoJournal(true)
			.useNoPrealloc(true)
			.build()).build();
		System.out.println("Récupération de l'exécutable");
		mongodExecutable = starter.prepare(mongodConfig);
		System.out.println("Lancement à la volée du serveur MongoDB");
		mongodProcess = mongodExecutable.start();
		System.out.println("Serveur MongoDB lancé avec succès");
	}

	/**
	 * Drop la base à la suite de chaque test
	 * @throws Exception
	 */
	@After
	public void after() throws Exception{
		System.out.println("Fin du test. Suppression de la base " + DB_NAME);
		MongoClient client = new MongoClient(IP, PORT);
		MongoDatabase db = client.getDatabase(DB_NAME);
		db.drop();
		client.close();
		System.out.println("Base " + DB_NAME + " supprimée avec succès");
	}

	/**
	 * Stop l'exécutable et le process mongod après l'exécution des tests
	 * @throws UnknownHostException
	 */
	@AfterClass
	public static void destroyMongodInstance() throws UnknownHostException {

		System.out.println("Arrêt du serveur MongoDB");
		if (mongodProcess != null) {
			mongodProcess.stop();
		}
		System.out.println("Arrêt de l'exécutable");
		if (mongodExecutable != null) {
			mongodExecutable.stop();
		}
		System.out.println("Fin des tests");
	}
}
