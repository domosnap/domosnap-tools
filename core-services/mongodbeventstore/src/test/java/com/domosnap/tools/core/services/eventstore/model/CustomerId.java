/* CustomerId
 * aggregate of the customer
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.tools.core.services.eventstore.model;

import java.util.UUID;

import com.domosnap.tools.core.services.cqrs.domain.AggregateId;

public class CustomerId implements AggregateId {
    private String value;
    public CustomerId(String value) {

        this.value = value;
    }

    public static CustomerId generate() {
        return new CustomerId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerId messageId = (CustomerId) o;

        return !(value != null ? !value.equals(messageId.value) : messageId.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value;
    }

	@Override
	public String getId() {
		return value;
	}
}
