package com.domosnap.tools.core.services.eventstore;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.tools.core.services.cqrs.domain.Event;
import com.domosnap.tools.core.services.eventstore.model.CustomerCreatedEvent;
import com.domosnap.tools.core.services.eventstore.model.CustomerId;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

/**
 * Classe de test de PersonneDao
 * @author ssenouci
 *
 */
public class MongoEventStoreTest extends AbstractMongoDbTest {

	/**
	 * Teste la création d'une personne 
	 * @throws UnknownHostException
	 */
	@Test
	public void testCreate() throws UnknownHostException {
		MongoDBEventStore e = new MongoDBEventStore();
		
		e.init(new HashMap<String, Object>());
		
		final CustomerId aggregateId = CustomerId.generate();
		
		Event<CustomerId> event = new CustomerCreatedEvent(aggregateId, 10, "externe", new ResourceName("//domain/path/id"));
 		
		
		e.store(event);
		
		List<Event<?>> result = e.getEventsOfAggregate(aggregateId.toString());
		
		Assert.assertNotNull(result);
		Assert.assertSame(1, result.size());
		
		Assert.assertEquals(aggregateId.toString(), result.get(0).getAggregateId().toString());
		Assert.assertEquals("path", ((CustomerCreatedEvent) result.get(0)).getCreator().getPath() );
	}
}
