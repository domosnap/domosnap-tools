/* CustomerCreatedEvent
 * Event for when a customer is created
 * Raphaël Lopes
 * 06.06.2020
 */
package com.domosnap.tools.core.services.eventstore.model;

import com.domosnap.tools.core.services.cqrs.domain.impl.EventBase;
import com.domosnap.tools.core.services.resourcenames.ResourceName;

public class CustomerCreatedEvent extends EventBase<CustomerId> {

	private final Integer idPartner;
	private final String externalId;

    public CustomerCreatedEvent(CustomerId customerId, Integer idPartner, String externalId,ResourceName creator) {
    	super(customerId, creator);
    	this.idPartner = idPartner;
		this.externalId = externalId;
    }
    
    public Integer getIdPartener() {
		return idPartner;
	}
	public String getExternalId() {
		return externalId;
	}
}
